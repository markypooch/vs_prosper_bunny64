#include "pch.h"
#include "CppUnitTest.h"

#include "..\vs_prosper_bunny64\bsystem\Graphics.h"
#include "..\vs_prosper_bunny64\bsystem\TextureManager.h"

#include <assert.h>

#include <IL\il.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace vsprosperbunny64testing
{
	TEST_CLASS(vsprosperbunny64testing)
	{
	public:
		TEST_METHOD(TestMethod_TextureManager_Instantiate)
		{
			auto textureManager = std::make_unique<bsystem::TextureManager>(nullptr);
			uint32_t id         = -1;
			try {
				id = textureManager->loadTexture(".\\test");
			}
			catch (std::runtime_error error) {
				assert(id == -1);
			}
		}

		TEST_METHOD(TestMethod_TextureManager_LoadTexture)
		{
			auto graphics       = std::make_shared<bsystem::Graphics>(800, 800);
			auto textureManager = std::make_unique<bsystem::TextureManager>(graphics);
			
			uint32_t id         = -1;
			id = textureManager->loadTexture(".\\test.png");

			assert(id != -1);
		}

		TEST_METHOD(TestMethod_TextureManager_LoadNotExistant)
		{
			auto graphics = std::make_shared<bsystem::Graphics>(800, 600);
			auto textureManager = std::make_unique<bsystem::TextureManager>(graphics);

			uint32_t id = -1;
			try {
				id = textureManager->loadTexture(".\\IDONTEXIST.png");
			}
			catch (std::runtime_error error) {
				assert(id == -1);
			}
		}
	};
}
