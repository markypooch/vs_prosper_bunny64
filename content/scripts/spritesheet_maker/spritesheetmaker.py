from PIL import Image
import os

def create_spritesheet():

    directory           = input("Enter directory with image files: ")
    number_of_sprites_x = int(input("Enter the number of sprites for each row: "))
    number_of_sprites_y = int(input("Enter the number of sprites for each column: "))

    x_offset            = int(input("Enter x offset for sprite sheet: "))
    y_offset            = int(input("Enter y offset for sprite sheet: "))

    x_size = number_of_sprites_x * x_offset
    y_size = number_of_sprites_y * y_offset

    sprite_sheet = Image.new("RGBA", (x_size, y_size), (0, 0, 0, 0))

    files        = os.listdir(directory)
    images       = []

    print("\nComputing the things...")
    for file in files: images.append(Image.open(f"{directory}\\{file}"))
    for y in range(number_of_sprites_y):
        for x in range(number_of_sprites_x):
            idx = (y + (x*number_of_sprites_x)) if number_of_sprites_x < number_of_sprites_y else (x + (y*number_of_sprites_x))
            if idx >= len(files):
                break

            img = images[idx] if number_of_sprites_x < number_of_sprites_y else images[idx]
            sprite_sheet.paste(img, 
                            (x*x_offset, y*y_offset), 
                            mask=img)
        if idx > len(files):
            break
    
    sprite_sheet_name = input("Enter the sprite sheet name: ")
    sprite_sheet.save(f"{sprite_sheet_name}.png", format="png")

if __name__ == "__main__":
    create_spritesheet()