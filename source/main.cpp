#include <GLFW/glfw3.h>
#include <ratio>
#include <chrono>

#include "game\IGameState.h"
#include "game\World.h"

#include "bsystem\Logger.h"
#include "bsystem\Graphics.h"
#include "bsystem\Input.h"

#define GLFW_HAND_CURSOR   0x00036004

using namespace std;
using namespace game;
using namespace bsystem;

using namespace std::chrono;

int main(void) {
	
	glfwSetErrorCallback(Logger::glfw_error_callback);
	if (!glfwInit()) {
		Logger::write_to_file("main.cpp", 
			"14", 
			"GLFW Failed to initialize", 
			"FATAL");
	}

	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	int resolution[] = { mode->width, mode->height };

	/* our pointer for our glfw window, uses older C API so no smart pointers */
	GLFWwindow* glfwWindow = nullptr;
	glfwWindow             = glfwCreateWindow(resolution[0], 
		                                      resolution[1], 
		                                      "Prosper Bunny", 
		                                      nullptr, 
		                                      nullptr);
	if (!glfwWindow) {
		Logger::write_to_file("main.cpp",
			"23",
			"Unable to create window (glfwCreateWindow)",
			"FATAL");
	}

	/* bind the GL context to our window */
	glfwMakeContextCurrent(glfwWindow);
	
	std::shared_ptr<Graphics> graphics = std::make_shared<Graphics>(resolution[0], resolution[1]);
	graphics->initialize();

	std::shared_ptr<TextureManager> textureManager = std::make_shared<TextureManager>(graphics);
	float deltaTime = 0.0f;

	std::shared_ptr<Input> input = std::make_shared<Input>(glfwWindow);

	/* instantiate our world */
	std::unique_ptr<IGameState> world = std::make_unique<World>(resolution, graphics, textureManager);
	world->initialize(input);
	while (!glfwWindowShouldClose(glfwWindow)) {

		high_resolution_clock::time_point t1 = high_resolution_clock::now();

		input->pollInput();

		world->update(deltaTime, input);
		world->render();

		glfwSwapBuffers(glfwWindow);

		high_resolution_clock::time_point t2 = high_resolution_clock::now();

		duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
		deltaTime = time_span.count();
	}

	glfwDestroyWindow(glfwWindow);
	glfwTerminate();
	return 0;
}