// 3rd Party Audio Dependencies
//////////////////////////////////////////////////////////////////////////////////////////

MiniAudio 0.9.5 (MIT ./dependencies/MiniAudio 0.9.5/LICENSE)

	Include Directory:
	./dependencies/MiniAudio 0.9.5/include
	./dependencies/MiniAudio 0.9.5/include/extras

	Library Directory: (NONE - HEADER ONLY)

	Headerfile: miniaudio.h
	Optional: dr_flac.h
	          dr_mp3.h
			  dr_wav.h
			  jar_mod.h
			  jar_xm.h
			  stb_vorbis.c

	Linkfile: (NONE - HEADER ONLY)

SteamAudio 17 (https://github.com/ValveSoftware/steam-audio/blob/master/LICENSE.md)

	Include Directory:
	./dependencies/SteamAudio 17/include

	Headerfile: phonon.h

	Windows Library Directory:
	./dependencies/SteamAudio 17/lib/Windows/x86
	./dependencies/SteamAudio 17/lib/Windows/x64

	Linkfile: phonon.lib

	Mac Library Directory:
	./dependencies/SteamAudio 17/lib/OSX

	Linkfile: libphonon.dylib

	Linux Library Directory:
	./dependencies/SteamAudio 17/lib/Linux/x86
	./dependencies/SteamAudio 17/lib/Linux/x64

	Linkfile: libphonon.so

	Android Library Directory:
	./dependencies/SteamAudio 17/lib/Android

	Linkfile: libphonon.so

///////////////////////////////////////////////////////////////////////////////////////////
//

// Audio Lifecycle
///////////////////////////////////////////////////////////////////////////////////////////

#include "./bsystem/Audio.h"

using namespace bsystem;

// Instantate the audio manager somewhere
Audio audio;

// Initialize will create an audio worker thread (Call Before Main Loop)
audio.Initialize();

// Release will stop and destroy all audio resources along with worker thread (Call After Main Loop)
audio.Release();

///////////////////////////////////////////////////////////////////////////////////////////
//

// Loading Sounds /////////////////////////////////////////////////////////////////////////

/* Audio::Sound::ID is the handle type for sound resources */

/* Initialize handle with 0 because that is always a null sound id */
Audio::Sound::ID sound_id = 0; 

/* You can load in wav, mp3, or flac files */
audio.LoadSound(&sound_id , "Somefile.flac");

/* This will unload the sound */
audio.UnloadSound(sound_id);


// Playing Sounds (Part One) //////////////////////////////////////////////////////////////

/* With a valid sound handle, you can simply play a sound */
audio.PlaySound(sound_id);

// Playing Sounds (Part Two) //////////////////////////////////////////////////////////////

/* Audio::PlaySound actually creates an Audio::Stream.            */
/* An audio stream is used internally by the audio manager        */
/* to help mix and position separate sounds together.             */
/* In other words, an Audio::Stream is the instance of the sound. */

// Create a handle to an Audio::Stream
Audio::Stream::ID stream_id = 0;

// To get a valid handle of the Audio::Stream one of two ways.
// You pass along a pointer to Audio::PlaySound
audio.PlaySound(sound_id, &stream_id);

// Or you can call Audio::CreateStream directly
audio->CreateStream(&stream_id, Audio::Stream::STREAM_ONCE, sound_id);

/* Note the difference between PlaySound and CreateStream is that CreateStream */
/* does not play the sound immediatly like PlaySound does.                     */
/* PlaySound is like fire and forget mentality.                                */

// Controlling Streams (Part One) /////////////////////////////////////////////////////////

/* Once you have a valid Stream::ID, you can now control how that stream works. */
audio.StopStream(stream_id); // Pauses the stream if it's playing
audio.PlayStream(stream_id); // Plays or resumes the stream if paused
audio.DestroyStream(stream_id); // Stop and releases the stream


// Controlling Streams (Part Two) /////////////////////////////////////////////////////////

/* If you have looping music, use Audio::Stream::STREAM_CONTINUOUS */
audio.CreateStream(&stream_id, Audio::Stream::STREAM_CONTINUOUS, sound_id);
// or
audio.PlaySound(sound_id, &stream_id, Audio::Stream::STREAM_CONTINUOUS);


// Controlling Streams (Part Three : 3D Positional Audio) /////////////////////////////////

/* You can enable 3D audio positioning with               */
/* STREAM_ONCE_POSITIONAL or STREAM_CONTINUOUS_POSITIONAL */
audio.CreateStream(&stream_id, Audio::Stream::STREAM_ONCE_POSITIONAL, sound_id);
// or
audio.PlaySound(sound_id, &stream_id, Audio::Stream::STREAM_ONCE_POSITIONAL);

/* To change the location of where the audio should be played (per update if moving) */
/* Note that Stream::Position coordinate system is OpenGL right-handed; where +z is  */
/* pointing towards the user and -z is going into the screen.                        */
audio->SetStreamPosition(stream_id, Audio::Stream::Position{ 0.0f, 0.0f, 0.0f });

/* If the virtual camera is the listener of the Audio::Stream(s), you set its position. */
/* Stream position should not equal the listener's position, otherwise audio clipping   */
/* will occurr (similar to near clipping in OpenGL).                                    */
/* Listener's position should be updated whenever the virtual camera moves.             */
audio->SetListenerPosition(Audio::Stream::Position {0.0f, 0.0f, 1.0f});

// Controlling Streams (Part Four : Volume) //////////////////////////////////////////////

/* Each stream has it's own volume setting; default is 1.0f; range [0.0, 1.0] */
audio.SetStreamVolume(stream_id, 0.75f);

/* Master volume is for all audio output; default is 1.0f; range [0.0, 1.0] */
audio.SetMasterVolume(0.5f);


// That's Pretty Much It : Thanks for Reading ///////////////////////////////////////////