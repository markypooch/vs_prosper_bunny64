// Author: Noel Williams
///////////////////////////////////////////////////////////////////////////////////

#include "Audio.h"

#include <string>
#include <vector>
#include <map>
#include <mutex>

using namespace bsystem;

//  
// SOURCE ONLY: Audio Resource Management /////////////////////////////////////////////
std::map<Audio::Sound::ID,  Audio::Sound>   sound_pool;
std::map<Audio::Stream::ID, Audio::Stream> stream_pool;

std::map<std::string, Audio::Sound::ID> resource_pool;

Audio::Stream::Position position_listener = {0.0f};

float master_volume = 1.0f;

bool GetSound(Audio::Sound::ID sound_id, Audio::Sound** sound)
{
	if (sound_pool.count(sound_id) > 0)
	{
		*sound = &sound_pool[sound_id];
		return true;
	}
	
	*sound = nullptr;
	return false;
}

bool GetStream(Audio::Stream::ID stream_id, Audio::Stream** stream)
{
	if (stream_pool.count(stream_id) > 0)
	{
		*stream = &stream_pool[stream_id];
		return true;
	}

	*stream = nullptr;
	return false;
}

inline void ReleaseStream(Audio::Stream* stream)
{
	if (stream->platform)
	{
		delete stream->platform;
		stream->platform = nullptr;
	}

	if (stream->input.raw)
	{
		delete[] stream->input.raw;
		stream->input.raw = nullptr;
	}

	if (stream->output.raw)
	{
		delete[] stream->output.raw;
		stream->output.raw = nullptr;
	}
}

inline float* OffsetRawPtr(Audio::Data* data, size_t sample_offset)
{
	if (data)
	{
		if (sample_offset < data->sample_size)
		{
			return (data->raw) + (data->channel_count * sample_offset);
		}
			
	}
	return nullptr;
}

/// SOURCE ONLY: Audio Resource Management /////////////////////////////////////////
/// 

//
// DEPENDENCY: MiniAudio /////////////////////////////////////////
#define MINIAUDIO_IMPLEMENTATION

#define MA_NO_DSOUND
#define MA_NO_WINMM

#define DR_FLAC_IMPLEMENTATION
#include <miniaudio/extras/dr_flac.h>  /* Enables FLAC decoding. */
#define DR_MP3_IMPLEMENTATION
#include <miniaudio/extras/dr_mp3.h>   /* Enables MP3 decoding. */
#define DR_WAV_IMPLEMENTATION
#include <miniaudio/extras/dr_wav.h>   /* Enables WAV decoding. */

#include <miniaudio/miniaudio.h>

ma_device_config platform_device_configuration;
ma_device        platform_device;
Audio::Stream    platform_buffer;
std::mutex       platform_mutex;
ma_uint32        platform_frame_size;

// NOTE: This callback is from another thread
//       Also I need to modify miniaudio.h to provide the correct available samples
void platform_callback_write_samples(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 available_samples)
{
	if (platform_buffer.state == Audio::Stream::STATE_ACTIVE)
	{
		//  Prevent platform output buffer overrun //////////////////////////////////////////////////////////////
		{
			uint32_t samples_pending_write = platform_buffer.sample_current - platform_buffer.sample_last;

			if(platform_buffer.sample_last > samples_pending_write)
			{
				float* buffer_begin_position = OffsetRawPtr(&platform_buffer.output, 0);
				float* buffer_last_position  = OffsetRawPtr(&platform_buffer.output, platform_buffer.sample_last);

				// Copy pending samples to the beginning of the buffer
				memcpy(buffer_begin_position, buffer_last_position, sizeof(float) * samples_pending_write * platform_buffer.output.channel_count);

				platform_buffer.sample_current = samples_pending_write;
				platform_buffer.sample_last    = 0;
			}
		}
		/// Prevent platform output buffer overrun //////////////////////////////////////////////////////////////

		//  Update audio manager as much as needed to fill platform's available samples /////////////////////////
		Audio* audio = static_cast<Audio*>(pDevice->pUserData);

		uint32_t update_count  = (available_samples / platform_frame_size);
		         update_count += (available_samples % platform_frame_size) > 0 ? 1 : 0;

		while (update_count != 0)
		{
			audio->Update();
			update_count--;
		}
		/// Update Audio Manager ////////////////////////////////////////////////////////////////////////////////

		assert(platform_buffer.sample_current < platform_buffer.sample_end);
		
		//  Update Audio Platform //////////////////////////////////////////////////////////////////////////////
		std::lock_guard<std::mutex> guard(platform_mutex);

		uint32_t write_amount = platform_buffer.sample_current - platform_buffer.sample_last;
		uint32_t write_end    = platform_buffer.sample_current + write_amount;

		assert(write_amount < platform_buffer.sample_max);

		float* platform_output = (float*)pOutput;
		float* platform_input  = (float*)pInput;  /* Unused. */

		uint32_t actual_samples = (available_samples >= write_amount) ? write_amount : available_samples;

		if (actual_samples != available_samples)
		{
			int dummy = 0;
		}

		// The next two lines is where the magic happens
		float* audio_content = OffsetRawPtr(&platform_buffer.output, platform_buffer.sample_last);

		memcpy(platform_output, audio_content, sizeof(float) * actual_samples * platform_buffer.output.channel_count);

		platform_buffer.sample_last += actual_samples;
		/// Update Audio Platform //////////////////////////////////////////////////////////////////////////////
	}
}
/// DEPENDENCY: MiniAudio /////////////////////////////////////////
///

//
// DEPENDENCY: SteamAudio /////////////////////////////////////////
#include <phonon.h>

struct Phonon
{	
	IPLhandle context;

	// The amount of frames (aka:samples) to be processed during one update tick.
	uint32_t frame_size;

	IPLAudioFormat audio_format_mono;
	IPLAudioFormat audio_format_sterio;
	IPLAudioFormat audio_format_surround_5_1;
	IPLAudioFormat audio_format_surround_7_1;

	IPLAudioFormat platform_output_format;
	IPLAudioBuffer platform_output_buffer;

	IPLRenderingSettings  render_settings;
	IPLSimulationSettings simulation_settings;
	
	IPLHrtfParams        binaural_settings_hrtf;
	IPLhandle            binaural_renderer;

	IPLhandle environment;
	IPLhandle environment_renderer;
	
	Phonon() 
	: context(nullptr)
	, frame_size(0)
	, audio_format_mono()
	, audio_format_sterio()
	, audio_format_surround_5_1()
	, audio_format_surround_7_1()
	, platform_output_format()
	, platform_output_buffer()
	, render_settings()
	, simulation_settings()
	, binaural_settings_hrtf()
	, binaural_renderer(nullptr)
	, environment(nullptr)
	, environment_renderer(nullptr)
	{
		audio_format_mono.channelLayoutType               = IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_SPEAKERS;
		audio_format_mono.channelLayout                   = IPLChannelLayout::IPL_CHANNELLAYOUT_MONO;
		audio_format_mono.numSpeakers                     = 1;
		audio_format_mono.speakerDirections               = nullptr;                                 // Only used if channelLayout is IPL_CHANNELLAYOUT_CUSTOM
		audio_format_mono.ambisonicsOrder                 = 0;                                       // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_mono.ambisonicsOrdering              = IPL_AMBISONICSORDERING_FURSEMALHAM;      // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_mono.ambisonicsNormalization         = IPL_AMBISONICSNORMALIZATION_FURSEMALHAM; // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_mono.channelOrder                    = IPL_CHANNELORDER_INTERLEAVED;
													      
		audio_format_sterio.channelLayoutType             = IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_SPEAKERS;
		audio_format_sterio.channelLayout                 = IPLChannelLayout::IPL_CHANNELLAYOUT_STEREO;
		audio_format_sterio.numSpeakers                   = 2;
		audio_format_sterio.speakerDirections             = nullptr;                                 // Only used if channelLayout is IPL_CHANNELLAYOUT_CUSTOM
		audio_format_sterio.ambisonicsOrder               = 0;                                       // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_sterio.ambisonicsOrdering            = IPL_AMBISONICSORDERING_FURSEMALHAM;      // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_sterio.ambisonicsNormalization       = IPL_AMBISONICSNORMALIZATION_FURSEMALHAM; // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_sterio.channelOrder                  = IPL_CHANNELORDER_INTERLEAVED;

		audio_format_surround_5_1.channelLayoutType       = IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_SPEAKERS;
		audio_format_surround_5_1.channelLayout           = IPLChannelLayout::IPL_CHANNELLAYOUT_FIVEPOINTONE;
		audio_format_surround_5_1.numSpeakers             = 6;
		audio_format_surround_5_1.speakerDirections       = nullptr;                                 // Only used if channelLayout is IPL_CHANNELLAYOUT_CUSTOM
		audio_format_surround_5_1.ambisonicsOrder         = 0;                                       // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_5_1.ambisonicsOrdering      = IPL_AMBISONICSORDERING_FURSEMALHAM;      // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_5_1.ambisonicsNormalization = IPL_AMBISONICSNORMALIZATION_FURSEMALHAM; // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_5_1.channelOrder            = IPL_CHANNELORDER_INTERLEAVED;

		audio_format_surround_7_1.channelLayoutType       = IPLChannelLayoutType::IPL_CHANNELLAYOUTTYPE_SPEAKERS;
		audio_format_surround_7_1.channelLayout           = IPLChannelLayout::IPL_CHANNELLAYOUT_SEVENPOINTONE;
		audio_format_surround_7_1.numSpeakers             = 8;
		audio_format_surround_7_1.speakerDirections       = nullptr;                                 // Only used if channelLayout is IPL_CHANNELLAYOUT_CUSTOM
		audio_format_surround_7_1.ambisonicsOrder         = 0;                                       // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_7_1.ambisonicsOrdering      = IPL_AMBISONICSORDERING_FURSEMALHAM;      // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_7_1.ambisonicsNormalization = IPL_AMBISONICSNORMALIZATION_FURSEMALHAM; // Ignored if channelLayoutType is IPL_CHANNELLAYOUTTYPE_SPEAKERS
		audio_format_surround_7_1.channelOrder            = IPL_CHANNELORDER_INTERLEAVED;

		platform_output_format                     = audio_format_sterio;    // TBD in Audio::Initialize : Default is sterio
		platform_output_buffer.format              = platform_output_format; // TBD in Audio::Initialize
		platform_output_buffer.numSamples          = 0;                      // TBD in Audio::Initialize                   
		platform_output_buffer.interleavedBuffer   = nullptr;                // TBD in Audio::Initialize                 
		platform_output_buffer.deinterleavedBuffer = nullptr;                // TBD in Audio::Initialize
		
		render_settings.samplingRate    = 0; // TBD in Audio::Initialize
		render_settings.frameSize       = 0; // TBD in Audio::Initialize
		render_settings.convolutionType = IPLConvolutionType::IPL_CONVOLUTIONTYPE_PHONON;

		//NOTE: Head-Related Transfer Functions (HRTFs) : Default Settings
		//      https://en.wikipedia.org/wiki/Head-related_transfer_function
		//      https://www.sofaconventions.org/mediawiki/index.php/SOFA_(Spatially_Oriented_Format_for_Acoustics)
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		memset(&binaural_settings_hrtf, 0, sizeof(IPLHrtfParams));
		binaural_settings_hrtf.type         = IPLHrtfDatabaseType::IPL_HRTFDATABASETYPE_DEFAULT;                       
		binaural_settings_hrtf.hrtfData     = nullptr;
		binaural_settings_hrtf.sofaFileName = IPLstring();
		/// ////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	
	~Phonon()
	{
		iplDestroyEnvironmentalRenderer(&environment_renderer);
		iplDestroyEnvironment(&environment);
		iplDestroyBinauralRenderer(&binaural_renderer);
		iplDestroyContext(&context);
		iplCleanup();
	}
};/// struct Phonon


struct PhononStream
{
	float* mono_buffer;

	IPLAudioFormat input_format;
	IPLAudioFormat output_format;

	IPLhandle effect_direct_sound;
	IPLhandle effect_binaural;

	PhononStream() 
	: mono_buffer(nullptr)
	, effect_direct_sound(nullptr)
	, input_format()
	, output_format()
	, effect_binaural(nullptr)
	{
	}

	~PhononStream()
	{
		iplDestroyBinauralEffect(&effect_binaural);
		iplDestroyDirectSoundEffect(&effect_direct_sound);

		if (mono_buffer)
		{
			delete[] mono_buffer;
			mono_buffer = nullptr;
		}
	}
};

Phonon* phonon = nullptr;

/// DEPENDENCY: SteamAudio /////////////////////////////////////////
///


//
// Get the hell out of here Windows API defines; you are fucking up my shit //
#ifdef PlaySound
#undef PlaySound
#endif
/// //////////////////////////////////////////////////////////////////////////
///

// TONE GENERATOR
////////////////////////////////////////////////////
//double tone_frequency  = 440;
//			
//static int samp_index = 0;
//for(int index = 0; index < samples_to_process; index++)
//{
//	double time = (index + samp_index) / (double)(platform.frequency);
//	float amplitude = float(sin(time * tone_frequency * NK_PI));
//
//	for(uint32_t channel = platform.output.channel_count; 
//		         channel < platform.output.channel_count;
//		         channel++)
//	{
//		destination[channel] = amplitude;
//	}
//	// Update output buffer write position //////
//	platform.sample_current++;
//	/// /////////////////////////////////////////
//}
//samp_index += samples_to_process;
/// ////////////////////////////////////////////////
/// TONE GENERATOR



Audio::Audio()
{
	// Don't allow more than one instance of AudioManager
	if(phonon) throw; 

	ma_result       result;
	ma_context      context;
	ma_device_info* pPlaybackDeviceInfos;
	ma_uint32       playbackDeviceCount;
	ma_device_info* pCaptureDeviceInfos;
	ma_uint32       captureDeviceCount;
	ma_uint32       iDevice;


	if (ma_context_init(NULL, 0, NULL, &context) != MA_SUCCESS)
	{
		printf("Failed to initialize context.\n");
		return;
	}

	result = ma_context_get_devices(&context, &pPlaybackDeviceInfos, &playbackDeviceCount, &pCaptureDeviceInfos, &captureDeviceCount);
	if (result != MA_SUCCESS) 
	{
		printf("Failed to retrieve device information.\n");
		return;
	}

	printf("Playback Devices\n");
	for (iDevice = 0; iDevice < playbackDeviceCount; ++iDevice) 
	{
		printf("    %u: %s\n", iDevice, pPlaybackDeviceInfos[iDevice].name);
	}

	printf("\n");

	printf("Capture Devices\n");
	for (iDevice = 0; iDevice < captureDeviceCount; ++iDevice) 
	{
		printf("    %u: %s\n", iDevice, pCaptureDeviceInfos[iDevice].name);
	}

	ma_context_uninit(&context);
}



Audio::~Audio()
{
	Release();
}



bool Audio::Initialize()
{
	if(phonon) return true; // Must call Audio::Release() in order to reinitialize audio manager

	//  Initalize the Platform : MiniAudio /////////////////////////////////////////////////////////////////////////////////////////////////////////
	platform_device_configuration                     = ma_device_config_init(ma_device_type_playback);
	//platform_device_configuration.playback.format   = Desired Output Format;
	//platform_device_configuration.playback.channels = Desired Output Channels;
	//platform_device_configuration.sampleRate        = Desired Output SampleRate;
	platform_device_configuration.dataCallback        = platform_callback_write_samples;
	platform_device_configuration.pUserData           = this; // This is to give the platform callback a means to call Audio::Update
	
	if (ma_device_init(NULL, &platform_device_configuration, &platform_device) == MA_SUCCESS)
	{
		//  Setup the output buffer /////////////////////////////////////////////////////////////////////////////////////////
		memset(&platform_buffer, 0, sizeof(Stream));

		platform_buffer.state = Stream::STATE_STOPPED;
		platform_buffer.type  = Stream::Type::STREAM_CONTINUOUS;

		platform_buffer.sample_rate = platform_device.playback.internalSampleRate;
		platform_buffer.sample_end  = platform_buffer.sample_rate; // One second buffer
		platform_buffer.sample_max  = platform_buffer.sample_rate; // One second buffer

		platform_buffer.output.layout        = Audio::Data::INTERLEAVED;
		platform_buffer.output.channel_count = platform_device.playback.internalChannels;
		platform_buffer.output.channel_mask  = platform_device.playback.internalChannelMap[0]; //TODO: Figure out this channel mask useage
		platform_buffer.output.sample_size   = platform_buffer.sample_max;
		platform_buffer.output.sample_rate   = platform_buffer.sample_rate;
		platform_buffer.output.raw           = new float[platform_buffer.output.channel_count * platform_buffer.sample_max];
		platform_buffer.output.state         = Audio::Data::AVAILABLE;
		/// Setup the output buffer ////////////////////////////////////////////////////////////////////////////////////////

		platform_frame_size = platform_device.playback.internalBufferSizeInFrames / platform_device.playback.internalPeriods;
	}
	else throw;
	/// Initalize the Platform : MiniAudio /////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//  Initialize Phonon : SteamAudio /////////////////////////////////////////////////////////////////////////////////////////////////////////////
	IPLerror phonon_result;

	phonon = new Phonon();

	if( iplCreateContext(nullptr, nullptr, nullptr, &(phonon->context)) == IPL_STATUS_SUCCESS )
	{
		phonon->frame_size = platform_frame_size;

		switch (platform_buffer.output.channel_count)
		{
			case 1:  phonon->platform_output_format = phonon->audio_format_mono;         break;
			case 2:  phonon->platform_output_format = phonon->audio_format_sterio;       break;
			case 6:  phonon->platform_output_format = phonon->audio_format_surround_5_1; break;
			case 8:  phonon->platform_output_format = phonon->audio_format_surround_7_1; break;
			default: throw;
		}
		
		//  Setup Output Buffer Information /////////////////////////////////////////////////////////////////////////////////////
		phonon->platform_output_buffer.format     = phonon->platform_output_format;
		phonon->platform_output_buffer.numSamples = phonon->frame_size;
	
		switch(platform_buffer.output.layout)
		{
		case Audio::Data::INTERLEAVED: // (struct{float[channel_max]})[sample_max]
			phonon->platform_output_buffer.format.channelOrder = IPLChannelOrder::IPL_CHANNELORDER_INTERLEAVED;
			phonon->platform_output_buffer.interleavedBuffer   = platform_buffer.output.raw; // This is determined every update
			phonon->platform_output_buffer.deinterleavedBuffer = nullptr;
			break;
		case Audio::Data::SEQUENCED:   // (struct{float[sample_max]})[channel_max]
			//TODO: Not Implemented
			throw;
			//phonon->platform_output_buffer.format.channelOrder = IPLChannelOrder::IPL_CHANNELORDER_DEINTERLEAVED;
			//phonon->platform_output_buffer.interleavedBuffer   = nullptr;
			//phonon->platform_output_buffer.deinterleavedBuffer = &platform_buffer.output.raw; // This is determined every update
			//break;
		}		

		//  Setup Renderers' Settings ///////////////////////////////////////////////////////////////////////////////////////////
		phonon->render_settings.samplingRate    = platform_buffer.sample_rate;
		phonon->render_settings.frameSize       = phonon->frame_size;
		phonon->render_settings.convolutionType = IPLConvolutionType::IPL_CONVOLUTIONTYPE_PHONON;

		//  Setup Simulation Settings ///////////////////////////////////////////////////////////////////////////////////////////
		memset(&(phonon->simulation_settings), 0, sizeof(IPLSimulationSettings)); // Not using 
		

		//  Initialize 3D Audio Positioning Renderer ////////////////////////////////////////////////////////////////////////////
		phonon_result = iplCreateBinauralRenderer(phonon->context, phonon->render_settings, phonon->binaural_settings_hrtf, &(phonon->binaural_renderer));
		if(phonon_result != IPL_STATUS_SUCCESS ) throw;
		
		//  Initialize Environment + Renderer //////////////////////////////////////////////////////////////////////////////////
		phonon_result = iplCreateEnvironment(phonon->context, nullptr, phonon->simulation_settings, nullptr, nullptr, &(phonon->environment));
		if (phonon_result != IPL_STATUS_SUCCESS) throw;

		phonon_result = iplCreateEnvironmentalRenderer( phonon->context, 
		                                                phonon->environment,
														phonon->render_settings,
														phonon->audio_format_mono,
														IPLSimulationThreadCreateCallback(nullptr),
														IPLSimulationThreadDestroyCallback(nullptr),
														&(phonon->environment_renderer));
		if (phonon_result != IPL_STATUS_SUCCESS) throw;
	}
	else throw;
	/// Initialize Phonon : SteamAudio /////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
	// Start the platforms' playback thread
	//////////////////////////////////////////////////////
	if (ma_device_start(&platform_device) != MA_SUCCESS) throw;
	else platform_buffer.state = Stream::STATE_ACTIVE;
	/// //////////////////////////////////////////////////
	
	return true;
}



bool Audio::Release()
{
	ma_device_stop(&platform_device);
	ma_device_uninit(&platform_device);

	auto  istream  = stream_pool.begin();
	while(istream != stream_pool.end())
	{
		ReleaseStream(&(istream->second));
		istream = stream_pool.erase(istream);
	}

	for (auto isound = sound_pool.begin(); isound != sound_pool.end(); isound++)
	{
		UnloadSound(isound->first);
	}
	sound_pool.clear();

	if(phonon)
	{
		delete phonon;
		phonon = nullptr;
	}

	ReleaseStream(&platform_buffer);

	return true;
}



void Audio::Update()
{
	assert(phonon != nullptr);	
	assert(platform_buffer.sample_current <= (platform_buffer.sample_max - phonon->frame_size)); // Buffer overrun check

	std::lock_guard<std::mutex> guard(platform_mutex);

	// Do some audio processing and write results into the destination buffer
	////////////////////////////////////////////////////////////////////////////////
	float* destination_buffer = OffsetRawPtr(&platform_buffer.output, platform_buffer.sample_current);
	assert(destination_buffer != nullptr);

	// Find and modify appropriate sound streams and mix into destination.
	////////////////////////////////////////////////////////////////////////////////
	static std::vector<IPLAudioBuffer> mix_pending;
	mix_pending.clear();
	uint32_t mix_max = 0;

	// Iterate and determine which audio streams are active for mixing.
	// NOTE: If a source is active it will increment the mix_max value.
	/////////////////////////////////////////////////////////////////////////////////
	auto  stream_iter  = stream_pool.begin();
	while(stream_iter != stream_pool.end())
	{
		Stream& stream = stream_iter->second;
		switch( stream.state )
		{
			case Stream::STATE_STOPPED:
			// Move along, nothing to hear here
			stream_iter++;
			break; /// case Stream::STATE_STOPPED:
						
			case Stream::STATE_ACTIVE:
			{
				//TODO: Possibly move buffer padding to Audio::CreateStream
				{
					// Determine remaining amount of unmodified source data is left for processing //
					uint32_t source_remaining = stream.sample_end - stream.sample_current;
					uint32_t source_max       = stream.sample_max - stream.sample_current;
					/// /////////////////////////////////////////////////////////////////////////////

					// Input stream size check and padding
					// All streams that need to be mix should be at samples_to_process size.
					// NOTE: source_max is set larger that source_end if the padding process occured previously
					/////////////////////////////////////////////////////////////////////////////////////////////
					if((source_remaining < phonon->frame_size) && (source_max < phonon->frame_size))
					{
						// We need to pad the input stream in order to do any mixing. ///////////////////////////
						////////////////////////////////////////////////////////////////////////////////////////
						size_t org_size = size_t(stream.sample_max) * size_t(stream.input.channel_count);
						size_t pad_size = (phonon->frame_size - source_remaining) * stream.input.channel_count;
						size_t new_size = org_size + pad_size;
						/// ////////////////////////////////////////////////////////////////////////////////////
						
						// Thee Allocate ///////////////////////
						float* new_stream = new float[new_size];
							
						// Determine if extra padded data should mirror data at the begining 
						// of the input stream. This is to prevent any data gaps in looping 
						// input streams.
						////////////////////////////////////////////////////////////////////////////////////////
						if((stream.type == Stream::STREAM_CONTINUOUS) ||
						   (stream.type == Stream::STREAM_CONTINUOUS_POSITIONAL))
						{
							// Copy source stream into new buffer //////////////////////////////////////////////
							memcpy(new_stream, stream.input.raw, sizeof(float) * org_size);
							memcpy(new_stream + (stream.sample_end * stream.input.channel_count), stream.input.raw, sizeof(float) * pad_size);
							/// ////////////////////////////////////////////////////////////////////////////////
						}
						else // Normal Padding /////////////////////////////////////////////////////////////////
						{
							// Copy source stream into new buffer and zero the rest ////////////////////////////
							memcpy(new_stream, stream.input.raw, sizeof(float) * org_size);
							memset(new_stream + (stream.sample_end * stream.input.channel_count), 0, sizeof(float) * pad_size);
							/// ////////////////////////////////////////////////////////////////////////////////
						}

						stream.sample_max = (uint32_t)(new_size) / stream.input.channel_count;
						/// ////////////////////////////////////////////////////////////////////////////////////
						/// Input stream loop check for end of stream padding

						// Remove and Replace Input Stream Data 
						delete[] stream.input.raw;
						stream.input.raw = new_stream;
						new_stream = nullptr;
					}
					/// ///////////////////////////////////////////////////////////////////////////////////////
					/// Input stream size check and padding
				}
				
				// Input Stream 3D Positional Location Effect
				//////////////////////////////////////////////
				PhononStream* phonon_stream = static_cast<PhononStream*>(stream.platform);
				assert(phonon_stream != nullptr);

				IPLAudioBuffer stream_input;

				stream_input.format               = phonon_stream->input_format;
				stream_input.numSamples           = phonon->frame_size;
				stream_input.interleavedBuffer    = OffsetRawPtr(&stream.input, stream.sample_current);
				stream_input.deinterleavedBuffer  = nullptr;

				IPLAudioBuffer mono_buffer;

				mono_buffer.format              = phonon->audio_format_mono;
				mono_buffer.numSamples          = phonon->frame_size;
				mono_buffer.interleavedBuffer   = phonon_stream->mono_buffer;
				mono_buffer.deinterleavedBuffer = nullptr;

				IPLAudioBuffer stream_output;

				stream_output.format              = phonon_stream->output_format;
				stream_output.numSamples          = phonon->frame_size;
				stream_output.interleavedBuffer   = stream.output.raw;
				stream_output.deinterleavedBuffer = nullptr;


				if((stream.type == Stream::STREAM_ONCE_POSITIONAL) ||
				   (stream.type == Stream::STREAM_CONTINUOUS_POSITIONAL))
				{
					assert(phonon_stream->effect_direct_sound);
					assert(phonon_stream->effect_binaural);
						
					IPLVector3 listener_location = 
					{
						position_listener.x,
						position_listener.y,
						position_listener.z
					};

					IPLSource source_location;

					source_location.position    = IPLVector3{ stream.position.x, stream.position.y, stream.position.z };
					source_location.right       = IPLVector3 { 1.0f,  0.0f,  0.0f }; // I don't think this matters for our purposes, we are not doing directivity
					source_location.up          = IPLVector3 { 0.0f,  1.0f,  0.0f }; // I don't think this matters for our purposes, we are not doing directivity
					source_location.ahead       = IPLVector3 { 0.0f,  0.0f, -1.0f }; // I don't think this matters for our purposes, we are not doing directivity
					source_location.directivity = {0}; // Not using this advance shit for now

					// 
					IPLDirectSoundPath direct_sound_path = iplGetDirectSoundPath( phonon->environment,
						                                                          listener_location,
																				  IPLVector3 {0.0f, 0.0f, -1.0f},
																				  IPLVector3 {0.0f, 1.0f,  0.0f},
																				  source_location,
																				  0.0f, // Don't need a source radius since we are not using occlusion
							                                                      IPLDirectOcclusionMode::IPL_DIRECTOCCLUSION_NONE,
							                                                      IPLDirectOcclusionMethod::IPL_DIRECTOCCLUSION_RAYCAST);

					IPLDirectSoundEffectOptions direct_sound_options;

					direct_sound_options.applyDistanceAttenuation = IPL_TRUE;
					direct_sound_options.applyAirAbsorption       = IPL_TRUE;
					direct_sound_options.applyDirectivity         = IPL_FALSE;
					direct_sound_options.directOcclusionMode      = IPLDirectOcclusionMode::IPL_DIRECTOCCLUSION_NONE;

					iplApplyDirectSoundEffect( phonon_stream->effect_direct_sound,
						                       stream_input,
											   direct_sound_path,
											   direct_sound_options,
											   mono_buffer );

					// IPLVector3 is Right-Hand coordinate system
					IPLVector3 biaural_position = 
					{
						listener_location.x + source_location.position.x,
						listener_location.y + source_location.position.y,
						listener_location.z + source_location.position.z
					};

					iplApplyBinauralEffect( phonon_stream->effect_binaural,
											phonon->binaural_renderer,
											mono_buffer,
											biaural_position,
											//IPLHrtfInterpolation::IPL_HRTFINTERPOLATION_NEAREST, 
											IPLHrtfInterpolation::IPL_HRTFINTERPOLATION_BILINEAR, // HIGHER CPU USAGE ALLEDGEDLY
											stream_output );
				}
				else
				{
					if (stream_input.format.numSpeakers == stream_output.format.numSpeakers)
					memcpy(
							stream_output.interleavedBuffer,
							stream_input.interleavedBuffer,
							sizeof(float) * phonon->frame_size * stream_input.format.numSpeakers
							);
					else assert(false);
				}
				/// //////////////////////////////////////////
				/// Input Stream 3D Positional Location Effect
				
				//  Apply Stream Volume //////////////////////////////////////////////////////////////////////
				{
					size_t max_index = size_t(phonon->frame_size) * size_t(stream_output.format.numSpeakers);
					for (size_t index = 0; index < max_index; index++)
					{
						float sample = *(stream_output.interleavedBuffer + index);
						sample *= stream.volume;
						*(stream_output.interleavedBuffer + index) = sample;
					}
				}
				/// Apply Stream Volume //////////////////////////////////////////////////////////////////////

				// Queue Stream Mix Structure
				/////////////////////////////////////////////////////////////////////
				mix_max++;

				if(mix_max > mix_pending.size())
					mix_pending.push_back(stream_output);
				else 
					mix_pending[mix_max - 1] = stream_output;
				/// /////////////////////////////////////////////////////////////////
				/// Queue Stream Mix Structure


				// Advance The Sample Index Position
				/////////////////////////////////////////////////////////////////////
				stream.sample_current += phonon->frame_size;

				if(stream.sample_current > stream.sample_end)
				{
					if((stream.type == Stream::STREAM_CONTINUOUS) ||
					   (stream.type == Stream::STREAM_CONTINUOUS_POSITIONAL))
					{
						stream.sample_current %= stream.sample_end; // This operation should place the index correctly
						stream.sample_last = stream.sample_current; // stream.sample_last variable is not used anyhow.
					}
					else /// stream.type == Stream::STREAM_ONCE || Stream::STREAM_ONCE_POSITIONAL
					{
						// Release this stream on next audio update cycle.
						stream.state = Stream::STATE_END_OF_STREAM;
						stream.sample_current  = stream.sample_end; // Force the index to the "end" position.
																	// NOTE: Padding extends past the stream.sample_end value.
																	//       The real array size is stream.sample_max value.
						stream.sample_last = stream.sample_current; // Last I checked, still not used.
					}
				}/// if(stream.sample_current >= stream.sample_end)
				else /// if(stream.sample_current < stream.sample_end) 
				{
					stream.sample_last = stream.sample_current; // Geez, stop wasting cycles man.
				}
				/// /////////////////////////////////////////////////////////////////
				/// Advance The Sample Index Position							

			} stream_iter++; break; /// case Stream::STATE_ACTIVE:

			case Stream::STATE_END_OF_STREAM:
			{
				switch(stream.type)
				{
					case Stream::STREAM_ONCE:            
					case Stream::STREAM_ONCE_POSITIONAL:
					{
						// Wipe this stream from existance //
						ReleaseStream(&stream);
						stream_iter = stream_pool.erase(stream_iter);
						/// /////////////////////////////////
					} break;

					case Stream::STREAM_CONTINUOUS:
					case Stream::STREAM_CONTINUOUS_POSITIONAL:
					{
						// Hypothetically this conditional should not happen, but...
						assert(false);
					} break;

					default:
					assert(false);
					break;
				}
							
			} break; /// case Stream::STATE_END_OF_STREAM:

			default:
			assert(stream_iter != stream_pool.end());
			break;
		}
	}/// while(source != stream_source.end())

	//  Mix all audio streams into final destination buffer
	if (mix_max)
	{
		assert(phonon->platform_output_buffer.format.channelOrder == IPLChannelOrder::IPL_CHANNELORDER_INTERLEAVED);
			
		phonon->platform_output_buffer.interleavedBuffer = destination_buffer;
		
		iplMixAudioBuffers(mix_max, mix_pending.data(), phonon->platform_output_buffer);
	}

	//  Apply Master Volume
	{
		size_t max_index = size_t(phonon->frame_size) * size_t(phonon->platform_output_format.numSpeakers);
		for (size_t index = 0; index < max_index; index++)
		{
			float sample = *(destination_buffer + index);
			sample *= master_volume;
			*(destination_buffer + index) = sample;
		}
	}

	// Finished Updating the Platform Buffer
	platform_buffer.sample_current += phonon->frame_size;

}/// void Audio::Update()


// This variable is only for Audio::CreateStream //
Audio::Stream::ID next_sound_id = 0;

bool Audio::LoadSound(Sound::ID* sound_id, const char* filename)
{
	if(!sound_id || !filename) return false;

	Sound::ID id;

	// Check if audio was made before
	if (resource_pool.count(filename) > 0)
	{
		// Get Existing Audio ID
		id = resource_pool[filename];
	}
	else
	{
		// Get Next Available Audio ID
		do
		{
			do { id = next_sound_id++; } while (sound_pool.count(id) != 0);
		} while (id == 0);

		resource_pool[filename] = id;
	}

	// This will allocate Sound class if one does not exist
	Sound& sound = sound_pool[id];

	// Check if sound data is already loaded
	if(sound.data.state == Audio::Data::State::UNAVAILABLE)
	{
		// Sound Setup ///////////////////////////////////////////////////////
		sound.type = Sound::RESOURCE;

		ma_decoder audio_decoder;

		//ma_decoder_config config_decoder = ma_decoder_config_init( ma_format::ma_format_f32, platform_buffer.output.channel_count, platform_buffer.output.sample_rate);
		ma_decoder_config config_decoder;
		ma_zero_object(&config_decoder);

		config_decoder.format     = ma_format::ma_format_f32;
		config_decoder.sampleRate = platform_buffer.output.sample_rate;

		if (ma_decoder_init_file(filename, &config_decoder, &audio_decoder) == MA_SUCCESS)
		{
			sound.data.layout = Audio::Data::Layout::INTERLEAVED;
			
			sound.data.channel_count = audio_decoder.outputChannels;
			sound.data.channel_mask  = audio_decoder.outputChannelMap[0]; //TODO: Fix this
			sound.data.sample_rate   = audio_decoder.outputSampleRate;
			sound.data.sample_size   = ma_uint32(ma_decoder_get_length_in_pcm_frames(&audio_decoder));
			
			sound.data.raw = new float[sound.data.sample_size * sound.data.channel_count];

			size_t read = ma_decoder_read_pcm_frames(&audio_decoder, sound.data.raw, sound.data.sample_size);

			ma_decoder_uninit(&audio_decoder);

			sound.data.state = Audio::Data::State::AVAILABLE;
		}
		else
		{
			memset(&sound, 0, sizeof(Sound));
			return false;
		}
	}

	*sound_id = id;
	return true;
}



bool Audio::PlaySound(Sound::ID sound_id, Stream::ID* stream_id, Stream::Type stream_type)
{
	Stream::ID official_stream_id = 0;
	if (CreateStream(&official_stream_id, stream_type, sound_id))
	{
		if(stream_id) *stream_id = official_stream_id;
		return PlayStream(official_stream_id);
	}
	return false;
}



bool Audio::UnloadSound(Sound::ID sound_id)
{
	Audio::Sound* sound = nullptr;

	if (GetSound(sound_id, &sound))
	{
		if (sound->data.state == Audio::Data::State::AVAILABLE)
		{
			delete [] sound->data.raw;
			sound->data.raw   = nullptr;
			sound->data.state = Audio::Data::State::UNAVAILABLE;
		}
		return true;
	}
	return false;
}






// This variable is only for Audio::CreateStream //
Audio::Stream::ID next_stream_id = 0;

bool Audio::CreateStream(Stream::ID* stream_id, Stream::Type stream_type, Sound::ID sound_id)
{
	Audio::Sound* sound = nullptr;
	
	if (GetSound(sound_id, &sound))
	{
		if (sound->data.state == Audio::Data::AVAILABLE)
		{
			Stream::ID id;

			// Get Next Available Stream ID ///////////////////////////////////////////////////////
			do
			{
				do { id = next_stream_id++; }
				while (stream_pool.count(id) != 0);
			}
			while(id == 0);

			// std::map will allocate Audio::Stream class; in which we can get a direct reference to it
			Stream& stream = stream_pool[id];

			// Setup Stream //////////////////////////////////////////////////////////////////////
			stream.state = Stream::State::STATE_INVALID;
			stream.type  = stream_type;

			stream.position = {0.0f};
			stream.volume   = 1.0f;
			
			stream.sample_rate         = sound->data.sample_rate;
			stream.sample_current      = 0;
			stream.sample_end          = sound->data.sample_size;
			stream.sample_max          = stream.sample_end;
			stream.sample_last         = 0;

			//  Initialize Input Buffer ////////////////////////////////////////////////////////////////////////////////
			stream.input.state         = Data::UNAVAILABLE;
			stream.input.layout        = sound->data.layout;
			stream.input.channel_count = sound->data.channel_count;
			stream.input.channel_mask  = sound->data.channel_mask;  //TODO: Fix this
			stream.input.sample_size   = sound->data.sample_size;
			stream.input.sample_rate   = sound->data.sample_rate;
			stream.input.raw           = new float[stream.input.sample_size * stream.input.channel_count];
			// Copy source audio into new buffer
			memcpy(stream.input.raw, sound->data.raw, sizeof(float) * stream.input.sample_size * stream.input.channel_count);
			stream.input.state = Data::AVAILABLE;
			/// Initialize Input Buffer ////////////////////////////////////////////////////////////////////////////////
		
			//  Initialize Output Buffer ///////////////////////////////////////////////////////////////////////////////
			stream.output.state         = Data::UNAVAILABLE;
			stream.output.layout        = platform_buffer.output.layout;
			stream.output.channel_count = platform_buffer.output.channel_count;
			stream.output.channel_mask  = platform_buffer.output.channel_mask;
			stream.output.sample_size   = platform_buffer.output.sample_rate;
			stream.output.sample_rate   = platform_buffer.output.sample_rate;
			stream.output.raw           = new float[stream.output.sample_size * stream.output.channel_count]; // Create a 1sec buffer
			// Clear output buffer
			memset(stream.output.raw, 0, sizeof(float) * stream.output.sample_size * stream.output.channel_count);
			stream.input.state = Data::AVAILABLE;
			/// Initialize Output Buffer ///////////////////////////////////////////////////////////////////////////////
		
			//  Stream's Effects Initialization ////////////////////////////////////////////////////////////////////////
			{
				PhononStream* stream_phonon = new PhononStream();
				stream.platform = stream_phonon;

				//  Create a stream effect buffer 
				//  This buffer is to temporaraly hold the results of any effect being processed.
				//  This buffer is 1 second in size and is always mono.
				/////////////////////////////////////////////////////////////////////////////////////////////////////////
				stream_phonon->mono_buffer = new float[platform_buffer.output.sample_rate];
				memset(stream_phonon->mono_buffer, 0, sizeof(float) * platform_buffer.output.sample_rate);
				/// Create a stream effect buffer ///////////////////////////////////////////////////////////////////////

				//  Common Input-Output Information /////////////////////////////////////////////////////////////////////
				switch (stream.input.channel_count)
				{
				case 1:  stream_phonon->input_format = phonon->audio_format_mono;         break;
				case 2:  stream_phonon->input_format = phonon->audio_format_sterio;       break;
				case 6:  stream_phonon->input_format = phonon->audio_format_surround_5_1; break;
				case 8:  stream_phonon->input_format = phonon->audio_format_surround_7_1; break;
				default: throw;
				}

				stream_phonon->output_format = phonon->platform_output_format;
				/// Common Input-Output Information /////////////////////////////////////////////////////////////////////

				IPLerror ipl_error_code;

				//  Initialize Direct Sound Effect  /////////////////////////////////////////////////////////////////////
				ipl_error_code = iplCreateDirectSoundEffect( phonon->environment_renderer,
					                                         stream_phonon->input_format,
					                                         phonon->audio_format_mono,
					                                         &(stream_phonon->effect_direct_sound));

				if (ipl_error_code != IPL_STATUS_SUCCESS) throw;
				/// Initialize Direct Sound Effect  /////////////////////////////////////////////////////////////////////

				//  Initialize Binaural Effect //////////////////////////////////////////////////////////////////////////
				ipl_error_code = iplCreateBinauralEffect( phonon->binaural_renderer,
													      phonon->audio_format_mono,
					                                      stream_phonon->output_format,
					                                      &(stream_phonon->effect_binaural) );

				if (ipl_error_code != IPL_STATUS_SUCCESS) throw;
				/// Initialize Binaural Effect //////////////////////////////////////////////////////////////////////////

			}
			/// Stream's Effects Initialization ////////////////////////////////////////////////////////////////////////
			
			stream.state = Stream::STATE_STOPPED;
			*stream_id = id;
			return true;
		}		
	}

	*stream_id = 0;
	return false;
}



bool Audio::PlayStream(Stream::ID stream_id)
{
	Stream* stream = nullptr;

	if(GetStream(stream_id, &stream))
	{
		if((stream->state == Stream::STATE_INVALID)||(stream->	state == Stream::STATE_END_OF_STREAM)) return false;
		stream->state = Stream::STATE_ACTIVE;
		return true;
	}

	return false;
}



bool Audio::StopStream(Stream::ID stream_id)
{
	Stream* stream = nullptr;

	if(GetStream(stream_id, &stream))
	{
		if((stream->state == Stream::STATE_INVALID)||(stream->state == Stream::STATE_END_OF_STREAM))
		{
			return false;
		}
		else
		{
			stream->state = Stream::STATE_STOPPED;
			return true;
		}
	}

	return false;
}



bool Audio::DestroyStream(Stream::ID stream_id)
{
	Stream* stream = nullptr;

	if(GetStream(stream_id, &stream))
	{
		ReleaseStream(stream);
		stream_pool.erase(stream_id);
		return true;
	}

	return false;
}




bool bsystem::Audio::SetListenerPosition(Stream::Position listener_position)
{
	position_listener = listener_position;
	return true;
}



bool bsystem::Audio::SetStreamPosition(Stream::ID stream_id, Stream::Position stream_position)
{
	Stream* stream = nullptr;

	if (GetStream(stream_id, &stream))
	{
		stream->position = stream_position;
		return true;
	}
	return false;
}



bool bsystem::Audio::SetMasterVolume(float volume)
{
	if (volume <= 0.0f) volume = 0.0f;
	if (volume >= 1.0f) volume = 1.0f;
	master_volume = volume;
	return true;
}




bool bsystem::Audio::SetStreamVolume(Stream::ID stream_id, float volume)
{
	Stream* stream = nullptr;

	if (GetStream(stream_id, &stream))
	{
		if(volume <= 0.0f) volume = 0.0f;
		if(volume >= 1.0f) volume = 1.0f;
		stream->volume = volume;
		return true;
	}
	return false;
}
