// Author: Noel Williams
#pragma once

#include <stdint.h>

/*
	Notes on Requirements

	No wrapping error results; just throw
	Secondary is logging

	Reduce switch statment usage

	Minimal public private statements

	Rename Audio to AudioManager
*/
namespace bsystem 
{
	class Audio
	{
	public:
		// NOTE: Data, Sound, and Stream structures are public;
		//       however the end user will not be able to access data in
		//       these structures since there is no public user interface(s)
		//       to get the data. Structure data and implementation are internal
		//       to the audio manager. These structures are to be used
		//       by the end user as namespace types only.

		struct Data
		{
			enum State : uint32_t
			{
				UNAVAILABLE,
				AVAILABLE
			} state; 

			enum Layout : uint32_t
			{
				INTERLEAVED, // (struct{float[channel_max]})[sample_max]
				SEQUENCED    // (struct{float[sample_max]})[channel_max]
			} layout;

			uint32_t channel_count;
			uint32_t channel_mask;

			uint32_t sample_size;
			uint32_t sample_rate;

			float* raw;

		};/// struct Data
		
		struct Sound
		{
			using ID = uint32_t;

			enum Type : uint32_t
			{
				RESOURCE, 
				GENERATED
			} type;

			Data data;

		};/// struct Sound

		struct Stream
		{
			using ID = uint32_t;

			struct Position
			{
				float x;
				float y;
				float z;
			};

			enum State : uint16_t
			{
				STATE_INVALID,
				STATE_STOPPED,
				STATE_ACTIVE,
				STATE_END_OF_STREAM
			};/// enum State

			enum Type : uint16_t
			{
				STREAM_INVALID,
				// Stream is unmodified and is destroyed apon end of stream. 
				STREAM_ONCE,
				// Stream is modified relative to its position and is destroyed apon end of stream. 
				STREAM_ONCE_POSITIONAL,
				// Stream is unmodified and repeats apon end of stream. 
				STREAM_CONTINUOUS,
				// Stream is modified relative to its position and repeats apon end of stream. 
				STREAM_CONTINUOUS_POSITIONAL
			};/// enum Type

			State state;
			Type  type;

			Stream::Position position; // Valid if type == STREAM_TYPE_POSITIONAL
			float volume;

			uint32_t sample_rate;

			uint32_t sample_current;
			uint32_t sample_end;
			uint32_t sample_max;
			uint32_t sample_last;

			Data input;  // Input : source sample data
			Data output; // Output: modified sample data

			void* platform; // Internal Platform Data

		};/// struct Stream
	
		Audio();
		~Audio();

		bool Initialize();
		bool Release();
		void Update();

		bool   LoadSound(Sound::ID* sound_id, const char* filename);
		bool   PlaySound(Sound::ID  sound_id, Stream::ID* stream_id = nullptr, Stream::Type stream_type = Stream::Type::STREAM_ONCE);
		bool UnloadSound(Sound::ID  sound_id);
		
		bool  CreateStream(Stream::ID* stream_id, Stream::Type stream_type, Sound::ID sound_id);
		bool    PlayStream(Stream::ID  stream_id);
		bool    StopStream(Stream::ID  stream_id);
		bool DestroyStream(Stream::ID  stream_id);

		bool SetListenerPosition(Stream::Position stream_position);
		bool SetStreamPosition(Stream::ID stream_id, Stream::Position stream_position);

		bool SetMasterVolume(float volume);
		bool SetStreamVolume(Stream::ID stream_id, float volume);

	};/// class Audio
};/// namespace bsystem 