#include "TextureManager.h"
#include "Logger.h"

#include <IL\il.h>
#include <IL\ilu.h>
#include <IL\ilut.h>

#include <exception>

using namespace bsystem;
using namespace std;

TextureManager::TextureManager(std::shared_ptr<Graphics> graphics) {
	this->graphics = graphics;

	graphics->enableTexture2D();
	ilInit();
}

TextureManager::~TextureManager() {

	//Unallocate our GL Textures
	/////////////////////////////////////////////////////
	for (auto const& texture : textureMap) {
		glDeleteTextures(1, &texture.second);
	}
}

uint32_t TextureManager::loadTexture(std::string fileName) {

	//Does this image already exist?
	////////////////////////////////////////////////////
	if (textureMap.count(fileName) == 0) {

		//Define our integer handles
		////////////////////////////////////////////////
		ILuint imageID = -1;

		//Define our success, error callbacks
		////////////////////////////////////////////////
		ILboolean success = false;
		ILenum    error;

		//Generate, Bind, and Load Our Image
		////////////////////////////////////////////////
		ilGenImages(1, &imageID);
		checkResult();

		ilBindImage(imageID);
		checkResult();

		success = ilLoadImage(fileName.c_str());
		if (success) {

			//Attempt to convert DevIL image from intermediary format
			///////////////////////////////////////////////////////////////////
			success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
			if (!success) {
				std::string message = "DevIL failed to convert image: " + std::string(fileName);
				Logger::write_to_file("TextureManager.cpp", "25", message, "FATAL");

				//Attempt to cleanup before throwing exception 
				///////////////////////////////////////////////////////////////
				ilDeleteImages(1, &imageID);

				throw std::runtime_error(message);
			}

			//Create our Texture
			///////////////////////////////////////////////////////////////////
			textureMap[fileName] = graphics->createTexture2D(ilGetInteger(IL_IMAGE_WIDTH),
				ilGetInteger(IL_IMAGE_HEIGHT), 
				ilGetInteger(IL_IMAGE_BPP), 
				ilGetInteger(IL_IMAGE_FORMAT), 
				ilGetData());
		}
		else {

			std::string message = "DevIL failed to load image: " + std::string(fileName);
			Logger::write_to_file("TextureManager.cpp", "20", message, "FATAL");

			throw std::runtime_error(message);
		}
	}
	
	return textureMap[fileName];
}

//check the returned result of the DevIL library
///////////////////////////////////////////////////////
void TextureManager::checkResult() {
	
	//if the returned result is not, "IL_NO_ERROR", iterate through, calling GetError to retrieve
	//The entire stack of errors that may have occurred (http://openil.sourceforge.net/tuts/tut_3/index.htm)
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (ilGetError() != IL_NO_ERROR) {
		std::string message = retrieveErrors();

		Logger::write_to_file("TextureManager.cpp", "46", message, "FATAL");
		throw std::runtime_error(message);
	}
}

std::string TextureManager::retrieveErrors() {

	std::string errorString = "";
	ILenum Error;
	while ((Error = ilGetError()) != IL_NO_ERROR) 
		errorString += " IL_ERROR > " + Error;
	
	return errorString;
}