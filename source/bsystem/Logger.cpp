#include "Logger.h"
#include <ctime>
#include <fstream>
using namespace bsystem;

void Logger::glfw_error_callback(int, const char* err_str) {
	Logger::write_to_file("GLFW CALLBACK", "GLFW CALLBACK", err_str, "FATAL");
}

void Logger::write_to_file(std::string fileName, std::string lineNumber, std::string message, std::string msgLvl) {
	auto epoch = std::time(nullptr);
	std::ofstream outFile;

	outFile.open("pb.log", std::ios_base::app);
	outFile << "Epoch[" << epoch << "]" << " File Name[" << fileName << "]" << " Line Number[" << lineNumber << "]" << " Level[" << msgLvl << "]" << " Message[" << message << "]";
}