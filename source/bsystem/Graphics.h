#pragma once
#include <GLFW\glfw3.h>

namespace bsystem {
	class Graphics
	{
	public:
		Graphics(int width, int height);
		~Graphics() {};
	public:
		void     initialize();
		void     enableTexture2D();
		uint32_t createTexture2D(int width,
			int height,
			int bpp,
			int imageFormat,
			unsigned char* data);
	private:
		int width, height;
	};
};
