#pragma once
#include <vector>
#include <cstdlib>

namespace bsystem {
	class Camera
	{
	public:
		static std::vector<float> isoTo2D(float pnt[]) {
			std::vector<float> isometricCoordinates;
			isometricCoordinates.push_back((2 * pnt[1] + pnt[0]) / 2); 
			isometricCoordinates.push_back((2 * pnt[1] - pnt[0]) / 2);
		    
			return isometricCoordinates;
		}
		static std::vector<float> toIso(float pnt[]) {
			std::vector<float> isometricCoordinates;
			isometricCoordinates.push_back(pnt[0] - pnt[1]);
			isometricCoordinates.push_back((pnt[0] + pnt[1]) / 2);

			return isometricCoordinates;
		}
		static std::vector<int> getTileCoordinates(std::vector<float> pnt, float tileHeight) {
			std::vector<int> tileCoordinates;
			tileCoordinates.push_back((int)abs((pnt[0] / tileHeight)));
			tileCoordinates.push_back((int)abs((pnt[1] / tileHeight)));

			return tileCoordinates;
		}
	};
};
