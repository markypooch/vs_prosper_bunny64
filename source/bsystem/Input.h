#pragma once
#include <GLFW/glfw3.h>
#include "..\\game\Item.h"
#include "../game/Sprite.h"

namespace bsystem {
	class Input
	{
	public:
		Input(GLFWwindow*);
		~Input();
	public:
		void pollInput();
	public:
		bool mouse_down = false;
		GLFWwindow* glfwWindow;
		double xPos, yPos;
		
		std::shared_ptr<game::IItem> currentSprite;
	};
};
