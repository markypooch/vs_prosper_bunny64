#pragma once
#include "Graphics.h"
#include <iostream>
#include <map>

namespace bsystem {
	class TextureManager
	{
	public:
		TextureManager(std::shared_ptr<Graphics> graphics);
		~TextureManager();
	public:
		uint32_t loadTexture(std::string fileName);
	private:
		void                       checkResult();
		std::string                retrieveErrors();
	private:
		std::shared_ptr<Graphics>       graphics;
		std::map<std::string, uint32_t> textureMap;
	};
};
