#pragma once
#include <iostream>

namespace bsystem {
	class Logger
	{
	private:
		Logger()  {};
		~Logger() {};

		Logger(const Logger&)         {};
		void operator=(const Logger&) {};
	public:
		static void glfw_error_callback(int, const char* err_str);
		static void write_to_file(std::string fileName, std::string lineNumber, std::string message, std::string msgLvl);
	};
};