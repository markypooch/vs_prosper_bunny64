#include "Input.h"
#include <iostream>
#include <GLFW/glfw3.h>
using namespace bsystem;

Input::Input(GLFWwindow* glfwWindow) {
	this->glfwWindow = glfwWindow;
	this->currentSprite = nullptr;
}

Input::~Input() {

}

void Input::pollInput() {

	mouse_down = false;
	xPos = 0;
	yPos = 0;
	glfwPollEvents();
	if (glfwGetMouseButton(glfwWindow, GLFW_MOUSE_BUTTON_1)) {
		mouse_down = true;
	}
}