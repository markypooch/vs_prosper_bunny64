#include "Graphics.h"
using namespace bsystem;



Graphics::Graphics(int width, int height) {
	this->height = height;
	this->width  = width;
}

void Graphics::initialize() {

	/* Enter Projection Mode, and setup our camera matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, width, height, 0.0, 0.0, 1.0);

	/* Enter modelview perspective, and load the identity matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* set our clear color to corn flour blue */
	glClearColor(0.4, 0.6, 0.8, 0.0);
}

void Graphics::enableTexture2D() {
	/* Enable 2D texture mapping, and blending */
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);

	/* Configure our blend function */
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

uint32_t Graphics::createTexture2D(int width, int height, int bpp, int imageFormat, unsigned char* data) {
	uint32_t textureID = -1;

	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Set texture interpolation method to use linear interpolation (no MIPMAPS)
	/////////////////////////////////////////////////////////////////////////////
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// Specify the texture specification
	glTexImage2D(GL_TEXTURE_2D,
		0,
		bpp,
		width,
		height,
		0,
		imageFormat,
		GL_UNSIGNED_BYTE,
		data);

	return textureID;
}