#pragma once
#include "Sprite.h"
#include "..\\bsystem\TextureManager.h"

namespace game {
	class Cloud : public Sprite
	{
	public:
		Cloud(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~Cloud() {};
	public:
		virtual void initialize();
		virtual void update(float);
	};
};
