#include "World.h"

#include "..\bsystem\Camera.h"
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <rapidjson/document.h>
#include <time.h>

#define NUMBER_OF_ITEMS 5

using namespace rapidjson;
using namespace game;
using namespace bsystem;

World::World(int resolution[], std::shared_ptr<Graphics> graphics, std::shared_ptr<TextureManager> textureManager) {
	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->graphics = graphics;
	this->textureManager = textureManager;
	this->bunnyTimer = 0.0f;
	srand((int)time(0));

	cells = std::make_shared<std::vector<std::shared_ptr<Cell>>>();
}

World::~World() {

}

void World::calculateOffsets(int x, int y, float position[], int cellOffsets[], int initialX, int initialY) {

	cellOffsets[0] = x * (cellImageXOffset/2);
	cellOffsets[1] = y * (cellImageYOffset/2);

	float initialXOffset = resolution[0] / initialX;
	float initialYOffset = resolution[1] / initialY;

	position[0] = initialXOffset + (cellOffsets[0] - cellOffsets[1]);
	position[1] = initialYOffset + ((cellOffsets[0] + cellOffsets[1]) / 2);
}

bool World::initialize(std::shared_ptr<bsystem::Input> input) {

	//Open our json file
	//////////////////////////////////////////////////////////
	std::ifstream inJSON;
	inJSON.open(".\\data\\world.json");

	//Read it into a stringstream, and retrieve the str
	//////////////////////////////////////////////////////////
	std::stringstream strStream;
	strStream << inJSON.rdbuf(); 
	std::string jsonStr = strStream.str(); 

	//Parse the jsonStr into a RapidJSON object
	//////////////////////////////////////////////////////////
	Document d;
	d.Parse(jsonStr.c_str());
	
	//Retrieve the worldwidth, worldheight, and worldmap
	//////////////////////////////////////////////////////////
	Value& vWorldHeight       = d["WorldHeight"];
	Value& vWorldWidth        = d["WorldWidth"];
	Value& vWorldMap          = d["WorldMap"];

	//Loop over the worldmap array
	//////////////////////////////////////////////////////////
	int worldWidth = vWorldWidth.GetInt();
	int worldHeight = vWorldHeight.GetInt();

	for (int y = 0; y < worldHeight; y++) {
		for (int x = 0; x < worldWidth; x++) {

			//Retrieve the cells value, and cast to string
			////////////////////////////////////////////////
			int cell_int             = vWorldMap[x + (y * worldHeight)].GetInt();
			std::string cell_int_str = std::to_string(cell_int);

			//Cross reference the cell value for it's tile
			//definition
			////////////////////////////////////////////////
			if (!d["TileDefinitions"].HasMember(cell_int_str.c_str()))
				continue;
			Value& tileDefinition    = d["TileDefinitions"][cell_int_str.c_str()];

			//Retreive our default resolution divider for scaling
			//the cells, should the resolution change, it will
			//need to be caught in a callback and done again
			//////////////////////////////////////////////////////////
			Value& vResolutionDivider = d["TileDefinitions"]["DefaultResolutionDivider"];
			if (tileDefinition.HasMember("resolution_divider")) {
				vResolutionDivider = tileDefinition["resolution_divider"];
			}
			
			cellImageXOffset = this->resolution[0] / vResolutionDivider.GetInt();
			cellImageYOffset = this->resolution[0] / vResolutionDivider.GetInt();

			int   cellOffsets[2]       = { 0, 0 };
			float position[2]          = { 0, 0 };
			calculateOffsets(x, y, position, cellOffsets, 2, 4);

			//Add a cell to our vector
			////////////////////////////////////////////////
			cells->push_back(std::make_shared<Cell>(cellImageXOffset/2,
				cellImageYOffset/2,
				resolution, 
				position, 
				textureManager, 
				tileDefinition["type"].GetString(),
				tileDefinition["texture"].GetString()));
			cells->at(cells->size() - 1)->initialize();
			
			//Ensure the reference is added to our layer map
			////////////////////////////////////////////////
			this->layers[tileDefinition["layer"].GetInt()].push_back(cells->at(cells->size() - 1));

			//If the tile definition has a child member
			////////////////////////////////////////////////
			if (tileDefinition.HasMember("child")) {

				Value& child = tileDefinition["child"];

				//And Overrides the default resolution divider
				/////////////////////////////////////////////////
				if (child.HasMember("resolution_divider")) {

					int resolutionDivider = child["resolution_divider"].GetInt();

					calculateOffsets(x, y, position, cellOffsets, 2, 4);
					cellImageXOffset = this->resolution[0] / resolutionDivider;
					cellImageYOffset = this->resolution[0] / resolutionDivider;
				}

				position[1] -= cellImageYOffset/2;
				
				auto vegatation = std::make_shared<Vegetation>(cellImageXOffset / 2,
					cellImageYOffset / 2,
					resolution,
					position,
					child["animate"].GetBool(),
					textureManager,
					child["texture"].GetString());

				//Add the child tile to the appropriate layer for rendering
				///////////////////////////////////////////////////////////////
				this->layers[child["layer"].GetInt()].push_back(vegatation);
				this->layers[child["layer"].GetInt()].at(this->layers[child["layer"].GetInt()].size() - 1)->initialize();
			}

			{


				//Cross reference the cell value for it's tile
				//definition
				////////////////////////////////////////////////
				if (!d["EntityDefinitions"].HasMember(cell_int_str.c_str()))
					continue;

				Value& entityDefinitions = d["EntityDefinitions"][cell_int_str.c_str()];

				//Retreive our default resolution divider for scaling
				//the cells, should the resolution change, it will
				//need to be caught in a callback and done again
				//////////////////////////////////////////////////////////
				Value& vResolutionDivider = d["EntityDefinitions"]["DefaultResolutionDivider"];
				if (entityDefinitions.HasMember("resolution_divider")) {
					vResolutionDivider = entityDefinitions["resolution_divider"];
				}

				//int xDiv = vResolutionDivider.GetInt();
				//int yDiv = vResolutionDivider.GetInt();

				cellImageXOffset = this->resolution[0] / 30;
				cellImageYOffset = this->resolution[0] / 30;

				int cellOffsets[2] = { 0, 0 };
				float position[2] = { 0, 0 };
				calculateOffsets(x, y, position, cellOffsets, 2, 4);

				position[1] -= cellImageYOffset / 2;

				//Our map to relate action textures to integer handles
				/////////////////////////////////////////////////////////
				std::map<std::string, std::map<std::string, uint32_t>> actionTextureIDs;
				if (cell_int == 100) {

					//Type is Bunny, grab actions
					/////////////////////////////////////////////////////
					Value& actions = entityDefinitions["actions"];

					//loop through each action
					/////////////////////////////////////////////////////
					for (int i = 0; i < actions.Size(); i++) {

						Value& action = actions[i];

						//Load Texture through manager
						/////////////////////////////////////////////////
						Value& texturePaths = action["texture_paths"];
						actionTextureIDs[std::string(action["action"].GetString())]["north"] = this->textureManager->loadTexture(texturePaths["north"].GetString());
					}

					//Add it to layer 2
					auto bunny = std::make_shared<Bunny>(cellImageXOffset / 2, cellImageYOffset / 2, resolution, position, textureManager, actionTextureIDs, cells);
					bunnies.push_back(bunny);
					this->layers[2].push_back(bunny);
					//this->layers[2].at(this->layers[2].size() - 1)->initialize();
				}
				if (cell_int == 101) {
					auto butterfly = std::make_shared<Butterfly>(cellImageXOffset / 2,
						cellImageYOffset / 2,
						resolution,
						position,
						textureManager,
						entityDefinitions["texture"].GetString(),
						cells);

					//Add the child tile to the appropriate layer for rendering
					///////////////////////////////////////////////////////////////
					this->layers[2].push_back(butterfly);
					//this->layers[2].at(this->layers[2].size() - 1)->initialize();
				}
			}
		}
	}

	cellImageXOffset = this->resolution[0] / 4;
	cellImageYOffset = this->resolution[1] / 12;
	float position[2] = { resolution[0] / 2, cellImageYOffset / 2 };
	hud = std::make_shared<Hud>(cellImageXOffset / 2,
		cellImageYOffset / 2,
		resolution,
		position,
		textureManager,
		"./assets/menu.png");
	this->layers[2].push_back(hud);
	this->layers[2].at(this->layers[2].size() - 1)->initialize();

	float positions[NUMBER_OF_ITEMS][2] = {
		{ (resolution[0] / 2) - (cellImageXOffset / 2)*2, resolution[1] - cellImageYOffset },
		{ (resolution[0] / 2) - (cellImageXOffset / 2)*1, resolution[1] - cellImageYOffset },
		{ (resolution[0] / 2)                       , resolution[1] - cellImageYOffset },
		{ (resolution[0] / 2) + cellImageXOffset / 2, resolution[1] - cellImageYOffset },
		{ (resolution[0] / 2) + (cellImageXOffset / 2) * 2, resolution[1] - cellImageYOffset },
	};
	std::string textureNames[NUMBER_OF_ITEMS] = {
		"./assets/items/selecthand.png",
		"./assets/items/item_platform_food.png",
		"./assets/items/item_platform_medicine.png",
		"./assets/items/item_platform_lovepotions.png",
		"./assets/items/item_platform_tnt.png"
	};

	cellImageXOffset = this->resolution[0] / 20;
	cellImageYOffset = this->resolution[0] / 20;

	std::shared_ptr<game::Sprite> selector = std::make_shared<SelectorHand>(cellImageXOffset, cellImageYOffset, resolution, position, textureManager, ".\\assets\\selected.png");

	for (int i = 0; i < NUMBER_OF_ITEMS; i++) {
		cellImageXOffset = this->resolution[0] / 15;
		cellImageYOffset = this->resolution[0] / 15;

		auto item = std::make_shared<ItemPlatform>(cellImageXOffset / 2, cellImageYOffset / 2, resolution, positions[i], textureManager, textureNames[i], input, selector, bunnies);
		this->layers[2].push_back(item);
		this->layers[2].at(this->layers[2].size() - 1)->initialize();
	}

	for (int i = 0; i < this->layers[2].size(); i++) {
		this->layers[2].at(i)->initialize();
	}

	return true;
}

void World::update(float dt, std::shared_ptr<bsystem::Input> input) {

	bunnyTimer += dt;
	if (bunnyTimer > 5.0f) {
		bunnyPoints = 10 * (bunnies.size());
		bunnyTimer = 0.0f;
	}
	hud->bunnyPoints = bunnyPoints;

	if (input->mouse_down) {

		cellImageXOffset = this->resolution[0] / 20;
		cellImageYOffset = this->resolution[0] / 20;

		glfwGetCursorPos(input->glfwWindow, &input->xPos, &input->yPos);

		float pnt[2];
		pnt[0] = this->resolution[0] / 2 - input->xPos;
		pnt[1] = (this->resolution[1] / 4 - (cellImageXOffset / 2)) - input->yPos;
		std::vector<int> cellIndices = bsystem::Camera::getTileCoordinates(bsystem::Camera::isoTo2D(pnt), 96);
		
		if (!(cellIndices.at(0) < 0 || cellIndices.at(0) > 9 || cellIndices.at(1) < 0 || cellIndices.at(1) > 9)) {
			if (input->currentSprite != nullptr) {
				//Add it to layer 2
				auto currentCell = cells->at(cellIndices.at(0) + (cellIndices.at(1) * 10));

				float position[2];
				const float* pos = currentCell->retrievePosition();
				
				input->currentSprite->position[0] = pos[0];
				input->currentSprite->position[1] = pos[1];

				this->layers[2].push_back(input->currentSprite);
				this->layers[2].at(this->layers[2].size() - 1)->initialize();

				input->currentSprite = nullptr;
			}
		}
	}

	std::vector<int> inactiveIndices;
	for (auto sprite : layers[3]) {
		sprite->update(dt);
	}
	for (int i = 0; i < layers[2].size(); i++) {
		layers[2].at(i)->update(dt);
		if (layers[2].at(i)->inactive) {
			inactiveIndices.push_back(i);
		}
	}

	for (int i = 0; i < inactiveIndices.size(); i++) {
		layers[2].erase(layers[2].begin() + (inactiveIndices.at(i)-i));
	}
}

void World::render() {
	glClear(GL_COLOR_BUFFER_BIT);
	for (auto sprite : layers[3]) {
		sprite->render();
	}
	for (auto sprite : layers[2]) {
		sprite->render();
	}
}
