#include "Bunny.h"
#include "..\bsystem\Camera.h"

#include <stdlib.h>
#include <string>
#include <time.h>
#include <iostream>
using namespace game;

Bunny::Bunny(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::map<std::string, std::map<std::string, uint32_t>> texturePaths, std::shared_ptr<std::vector<std::shared_ptr<Cell>>> cells)
{
	this->textureHandles = texturePaths;
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0] - width;
	this->position[1] = position[1] - height;

	this->textureManager = textureManager;
	this->texturePath = texturePath;
	this->cells = cells;
	
	this->direction = Bunny::Direction::NORTH;

	this->u = 0;
	this->v = 0;
	this->dt = 0.0f;
	this->deathAnimationTimer = 0.0f;

	this->renderSheet = true;
	this->cellWidth = 1.0f / (6);
	this->cellHeight = 1.0f / (5);
	this->frame_x = 0;
	this->frame_y = 0;
	this->type = "bunny";
	this->flip = false;
	srand((int)time(0));

	this->bunny_happy = 100.0f;
	this->bunny_hunger = 0.0f;
	this->bunny_overall_health = 100.0f;
}

Bunny::~Bunny() {

}

void Bunny::initialize() {

	std::vector<int> cellIndices = getCellIndices(this->position[0], this->position[1]);
	current_cell = cells->at(cellIndices.at(0) + (cellIndices.at(1) * 10));

	actionList.root = std::make_shared<ActionList::Action>();
	actionList.root->behavior = ActionList::LOOK_AROUND;
	actionList.addNode(ActionList::Behaviors::SNIFF);
	actionList.addNode(ActionList::Behaviors::SCRATCHING);
	actionList.addNode(ActionList::Behaviors::WALKING);
}

std::vector<int> Bunny::getCellIndices(float posX, float posY) {
	float bunnyPosition[2];
	bunnyPosition[0] = resolution[0] / 2 - posX;
	bunnyPosition[1] = (resolution[1] / 4 - (height)) - posY;

	std::vector<float> pos;
	pos.push_back(bunnyPosition[0]);
	pos.push_back(bunnyPosition[1]);

	std::vector<int> indices = bsystem::Camera::getTileCoordinates(bsystem::Camera::isoTo2D(bunnyPosition), 96);
	return indices;
}

std::vector<int> Bunny::testgetCellIndices(float posX, float posY) {
	float bunnyPosition[2];
	bunnyPosition[0] = resolution[0] / 2 - posX;
	bunnyPosition[1] = (resolution[1] / 4 - (height)) - posY;

	std::vector<float> pos;
	pos.push_back(bunnyPosition[0]);
	pos.push_back(bunnyPosition[1]);

	std::vector<int> indices = bsystem::Camera::getTileCoordinates(pos, 96);
	return indices;
}

void Bunny::update(float dt) {

	if (dt > 0.1) dt = 0.1;

	
	if (flip) {
		this->renderSheet = false;
		int currentDirection = this->direction;
		std::string filePath = "./assets/bunny/blownup.png";
		this->textureHandle = this->textureManager->loadTexture(filePath);

		this->deathAnimationTimer += dt;
		if (this->deathAnimationTimer > 5.0f) {
			this->inactive = true;
			return;
		}
	}

	bunny_hungry_timer += dt;
	if (bunny_hungry_timer > 30.0f) {
		bunny_hungry_timer += 10.0f;
		bunny_hungry_timer = 0.0f;
	}

	ActionList::Behaviors behavior = actionList.traverseList();
	switch (behavior) {
	case ActionList::FLIPPING:
		break;
	case ActionList::WALKING:
		brainlessMove(dt);
		break;
	case ActionList::LOOK_AROUND:
		logicLessAction(dt, 0.03f);
		break;
	case ActionList::SCRATCHING:
		logicLessAction(dt, 0.03f);
		break;
	case ActionList::SNIFF:
		logicLessAction(dt, 0.03f);
		break;
	}
}

void Bunny::setAtlas() {
	
	int currentBehavior = this->actionList.currentAction->behavior;
	int currentDirection = this->direction;

	std::string filePath = "./assets/bunny/" + std::to_string(currentBehavior) + "/" + std::to_string(currentDirection) + ".png";
	this->textureHandle = this->textureManager->loadTexture(filePath);
}

bool Bunny::logicLessAction(float dt, float framestep) {

	if (!this->actionList.currentAction->initialCheck) {
		if ((rand() % 100 + 1) < 10 && current_cell->retrieveCellType() != "water") {
			setAtlas();
			this->frame_x = 0;
			this->frame_y = 0;
			this->u = 0;
			this->v = 0;
			this->actionList.currentAction->initialCheck = true;
			this->renderSheet = true;
		}
		else {
			this->actionList.currentAction->current_state = ActionList::State::FAILED;
			return false;
		}
	}

	if (!animate(dt, 50, 7, 6, framestep)) {
		this->actionList.currentAction->current_state = ActionList::State::RUNNING;
	}
	else {
		this->actionList.currentAction->current_state = ActionList::State::COMPLETE;
		this->actionList.currentAction->initialCheck = false;
	}
	return true;
}

bool Bunny::flipping(float dt) {

	position[0] += 0.1f;
	position[1] -= 1.5f;
	if (animate(dt, 30, 5, 4, 0.05f)) {
		this->inactive = true;
		return true;
	}
}

bool Bunny::brainlessMove(float dt) {
	
	if (!this->targetCell) {

		int tileLookups[8][3] = {
			{0,-1, Direction::NORTH},      {0,  1, Direction::SOUTH},      {1, 0, Direction::EAST},         {-1, 0, Direction::WEST}, 
		    {1,-1, Direction::NORTH_EAST}, {-1,-1, Direction::NORTH_WEST}, {-1, 1, Direction::SOUTH_WEST},  {1, 1,  Direction::SOUTH_EAST}
		};	

		int tile = rand() % 8;
		std::vector<int> cellIndices = getCellIndices(this->position[0], this->position[1]);

		current_cell = cells->at(cellIndices.at(0) + (cellIndices.at(1) * 10));

		int cellTargetX = abs((cellIndices.at(0) + tileLookups[tile][0]));
		int cellTargetY = abs((cellIndices.at(1) + tileLookups[tile][1]));

		if (cellTargetX > 9 || cellTargetY > 9 || cellTargetX < 0 || cellTargetY < 0) {
			this->actionList.currentAction->current_state = ActionList::State::FAILED;
			return false;
		}
		std::cout << cellTargetX << " " << cellTargetY << std::endl;
		targetCell = cells->at(cellTargetX + (cellTargetY * 10));
		const float* pos = targetCell->retrievePosition();

		this->distanceX = pos[0] - position[0];
		this->distanceY = pos[1] - position[1];

		targetIndices = getCellIndices(pos[0], pos[1]);
		this->direction = (Direction)tileLookups[tile][2];

		this->u = 0;
		this->v = 0;

		current_cell->sprites.push_back(std::shared_ptr<Sprite>(this));

		if (current_cell->retrieveCellType() == "water") {
			this->renderSheet = false;
			int currentDirection = this->direction;

			std::string filePath = "./assets/bunny/" + std::to_string(ActionList::TREADING) + "/" + std::to_string(currentDirection) + ".png";
			this->textureHandle = this->textureManager->loadTexture(filePath);
		}
		else {
			this->renderSheet = true;
			setAtlas();
			this->frame_x = 0;
			this->frame_y = 0;
		}
	}
	else {
		const float *pos = targetCell->retrievePosition();

		float movement_delta = dt / 4;
		int posX = pos[0];
		int posY = pos[1];
		
		if (position[0] <  posX) position[0] += (distanceX)*movement_delta;
		else if (position[0] > posX) position[0] -= -((distanceX)*movement_delta);

		if (position[1] <  posY)  position[1] += (distanceY)*movement_delta;
		else if (position[1] > posY) position[1] -= -((distanceY)*movement_delta);
		
		if (this->renderSheet) {
			animate(dt, 30, 5, 4, 0.009f);
		}

		if (checkInBounds()) {
			this->actionList.currentAction->current_state = ActionList::State::COMPLETE;
			targetCell = nullptr;

			this->frame_x = 0;
			this->frame_y = 0;
		}
		else				 this->actionList.currentAction->current_state = ActionList::State::RUNNING;
	}
	return true;
}

bool Bunny::checkInBounds() {
	std::vector<int> cellIndices = getCellIndices(this->position[0], this->position[1]);
	if (cellIndices[0] == targetIndices.at(0) && cellIndices[1] == targetIndices.at(1)) {
		std::cout << std::endl;
		std::cout << std::endl;
		return true;
	}
	else return false;
}

bool Bunny::eat(float dt) {
	return true;
}

bool Bunny::makeSweetLove(float dt) {
	return true;
}