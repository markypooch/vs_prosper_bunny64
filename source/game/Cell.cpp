#include "Cell.h"

using namespace game;

Cell::Cell(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::string cellType, std::string texturePath)
{
	this->cellType = cellType;
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;
}

Cell::~Cell() {

}

const float* Cell::retrievePosition() const {
	return position;
}

void Cell::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());
	return;
}

const std::string Cell::retrieveCellType() const {
	return cellType;
}

void Cell::update(float dt) {

}

void Cell::render() {
	Sprite::render();
}