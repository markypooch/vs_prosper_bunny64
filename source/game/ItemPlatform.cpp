#include "ItemPlatform.h"

#include "Bunny.h"
#include "Food.h"
#include "Bomb.h"
#include "Medicine.h"
#include "LovePotion.h"
using namespace game;

ItemPlatform::ItemPlatform(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::string texturePath, std::shared_ptr<bsystem::Input> input, std::shared_ptr<Sprite> sprite, std::vector<std::shared_ptr<Bunny>> bunnies) {
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;

	this->dt = 0;
	this->frame_x = 0;
	this->frame_y = 0;
	this->u = 0;
	this->v = 0;
	this->cellWidth = 1.0f / (6);
	this->cellHeight = 1.0f / (5);

	this->bunnies = bunnies;
	this->input = input;
	this->selector = selector;
}

void ItemPlatform::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());
}

void ItemPlatform::update(float dt) {
	if (input->mouse_down) {
		glfwGetCursorPos(input->glfwWindow, &input->xPos, &input->yPos);

		if (input->xPos < this->position[0] + width && input->xPos > this->position[0] - width && input->yPos < this->position[1] + height && input->yPos > this->position[1] - height) {
			std::cout << "ITEM HIT!!!!!" << std::endl;
			
			if (texturePath == "./assets/items/item_platform_food.png")        input->currentSprite = std::make_shared<Food>(this->resolution[0]/50, this->resolution[0]/50, resolution, position, textureManager, ".\\assets\\food.png");
			if (texturePath == "./assets/items/item_platform_medicine.png")    input->currentSprite = std::make_shared<Medicine>(this->resolution[0] / 50, this->resolution[0] / 50, resolution, position, textureManager, ".\\assets\\medicine.png");
			if (texturePath == "./assets/items/item_platform_lovepotions.png") input->currentSprite = std::make_shared<LovePotion>(this->resolution[0] / 50, this->resolution[0] / 50, resolution, position, textureManager, ".\\assets\\lovepotion.png");
			if (texturePath == "./assets/items/item_platform_tnt.png")         input->currentSprite = std::make_shared<Bomb>(this->resolution[0] / 30, this->resolution[0] / 30, resolution, position, textureManager, ".\\assets\\blast.png", bunnies);
		}
	}
}