#include "Sprite.h"
#include "..//bsystem/Input.h"
#include <vector>
#include "Bunny.h"

namespace game {
	class ItemPlatform : public Sprite {
	public:
		ItemPlatform(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath,
			std::shared_ptr<bsystem::Input> input,
			std::shared_ptr<Sprite> selector,
			std::vector<std::shared_ptr<Bunny>> bunnies);
		virtual ~ItemPlatform() {};
	public:
		virtual void initialize();
		virtual void update(float);
	private:
		std::shared_ptr<bsystem::Input> input;
		std::shared_ptr<Sprite> sprite;
		std::shared_ptr<Sprite> selector;
		std::vector<std::shared_ptr<Bunny>> bunnies;
	};
};