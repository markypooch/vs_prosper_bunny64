#pragma once
#include "..\\bsystem\Graphics.h"
#include "..\\bsystem\TextureManager.h"
#include <functional>

namespace game {
	class Sprite
	{
	public:
		virtual ~Sprite() {};
	public:
		virtual void        initialize()  = 0;
		virtual void        update(float) = 0;
		void         render();
	public:
		std::string type = "";
		bool inactive = false;
		float position[2];
	protected:
		bool renderSheet = false;
		
		float dt;

		double u, v;
		double cellWidth, cellHeight;
		int frame_x, frame_y;

		int resolution[2];
		
		int width, height;
		int textureHandle;
		std::shared_ptr<bsystem::TextureManager> textureManager;
		
		std::string                texturePath;
		std::function<void(float)> callback;
	protected:
		bool animate(float, int, int, int, float frametime);
	};
};
