#pragma once
#include "Item.h"
#include "Bunny.h"
#include "..\\bsystem\TextureManager.h"

namespace game {
	class Bomb : public IItem
	{
	public:
		Bomb(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath,
			std::vector<std::shared_ptr<Bunny>>                   bunnies);
		virtual ~Bomb();
	public:
		virtual void initialize();
		virtual void update(float);
		virtual int  consume() { return 0;  }
		virtual std::string type() { return "bomb"; }
	private:
		std::vector<std::shared_ptr<Bunny>>                   bunnies;
		bool active = false;
	};
};

