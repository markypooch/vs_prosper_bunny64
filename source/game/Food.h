#pragma once
#include "Sprite.h"
#include "..\\bsystem\TextureManager.h"
#include "Item.h"

namespace game {
	class Food : public IItem
	{
	public:
		Food(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~Food() {};
	public:
		virtual void initialize();
		virtual void update(float);
		virtual int  consume();
		virtual std::string type() { return "food"; }
	};
};