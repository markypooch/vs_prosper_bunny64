#pragma once
#include <memory>
#include <vector>

#include "IGameState.h"
#include "Cell.h"
#include "Cloud.h"
#include "Bunny.h"
#include "Vegetation.h"
#include "hud.h"
#include "ItemPlatform.h"
#include "Food.h"
#include "Medicine.h"
#include "LovePotion.h"
#include "SelectorHand.h"

#include "..\bsystem\Graphics.h"
#include "..\bsystem\TextureManager.h"
#include "Butterfly.h"
#include "..\bsystem\Logger.h"
#include "..\bsystem\Input.h"
#include "..\game\Bomb.h"

namespace game
{
	class World : public IGameState
	{
	public:
		World(int resolution[], 
			  std::shared_ptr<bsystem::Graphics> graphics, 
			  std::shared_ptr<bsystem::TextureManager> textureManager);
		~World();
	private:
		const World(World&) {};
	public:
		virtual bool initialize(std::shared_ptr<bsystem::Input> input);
		virtual void update(float, std::shared_ptr<bsystem::Input> input);
		virtual void render();
	private:
		void calculateOffsets(int x, int y, float position[], int cellOffsets[], int, int);
	private:
		int bunnyPoints = 0;
		int numberOfItems = 3;

		int cellImageXOffset = 0;
		int cellImageYOffset = 0;

		int resolution[2];
		float bunnyTimer;
		std::map<int, std::vector<std::shared_ptr<Sprite>>>   layers;
		std::shared_ptr<Hud>                                  hud;
		std::vector<std::shared_ptr<Bunny>>                   bunnies;
		std::vector<std::shared_ptr<Vegetation>>              vegetation;
		std::shared_ptr<std::vector<std::shared_ptr<Cell>>>                    cells;
		std::vector<std::shared_ptr<Cloud>>                   clouds;
		std::vector<std::shared_ptr<Butterfly>>               butterflys;

		std::shared_ptr<bsystem::Graphics>                    graphics;
		std::shared_ptr<bsystem::TextureManager>              textureManager;
	};
};
