#pragma once
#include <memory>
#include <iostream>
#include <vector>

#include "..\bsystem\TextureManager.h"
#include "Sprite.h"
#include "Item.h"

namespace game {
	
	class Cell : public Sprite
	{
	public:
		Cell(int  width,
			int   height,
			int   resolution[], 
			float position[], 
			std::shared_ptr<bsystem::TextureManager> textureManager, 
			std::string cellType,
			std::string texturePath);
		virtual ~Cell();
	public:
		const   float* retrievePosition() const;
		const   std::string retrieveCellType() const;

		virtual void initialize();
		virtual void update(float);
		void         render();
	public:
		std::vector<std::shared_ptr<Sprite>> sprites;
		std::vector<std::shared_ptr<IItem>>  items;
	private:
		std::string                          cellType;
	};
};