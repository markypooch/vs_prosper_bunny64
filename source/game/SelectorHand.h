#pragma once
#include "Sprite.h"
#include "..\\bsystem\TextureManager.h"

namespace game {
	class SelectorHand : public Sprite
	{
	public:
		SelectorHand(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~SelectorHand() {};
	public:
		virtual void initialize();
		virtual void update(float);
	};
};
