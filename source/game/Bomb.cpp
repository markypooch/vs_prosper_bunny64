#include "Bomb.h"

using namespace game;

Bomb::Bomb(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::string texturePath, std::vector<std::shared_ptr<Bunny>> bunnies) {
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;

	this->bunnies = bunnies;
	this->renderSheet = true;
	this->dt = 0;
	this->frame_x = 0;
	this->frame_y = 0;
	this->u = 0;
	this->v = 0;
	this->cellWidth = 1.0f / (8);
	this->cellHeight = 1.0f / (8);
}


Bomb::~Bomb() {

}

void Bomb::update(float dt) {
	
	if (!active) {
		for (int i = 0; i < bunnies.size(); i++) {
			if (bunnies.at(i)->position[0] > this->position[0] - 100 && bunnies.at(i)->position[0] < this->position[0] + 100 &&
				bunnies.at(i)->position[1] > this->position[1] - 100 && bunnies.at(i)->position[1] < this->position[1] + 100) {
				bunnies.at(i)->actionList.root->behavior = Bunny::ActionList::FLIPPING;
				if (bunnies.at(i)->actionList.currentAction != nullptr) {
					bunnies.at(i)->actionList.currentAction->current_state = Bunny::ActionList::State::FAILED;
				}
				bunnies.at(i)->flip = true;

			}
		}
		active = true;
	}
	if (animate(dt, 81, 8, 8, 0.01f)) {
		inactive = true;
	}
}

void Bomb::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());
}