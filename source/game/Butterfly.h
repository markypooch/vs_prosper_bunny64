#pragma once
#include <vector>
#include <string>
#include <random>

#include "Cell.h"
#include "Sprite.h"

namespace game {
	class Butterfly : public Sprite
	{
	public:
		Butterfly(int width,
			int height,
			int resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager>               textureManager,
			std::string texturePath,
			std::shared_ptr<std::vector<std::shared_ptr<Cell>>>                     cells);
		virtual ~Butterfly();
	public:
		enum Direction {
			NORTH = 0,
			SOUTH = 1,
			EAST = 2,
			WEST = 3,
			NORTH_EAST = 4,
			NORTH_WEST = 5,
			SOUTH_EAST = 6,
			SOUTH_WEST = 7
		} direction;

		virtual void initialize();
		virtual void update(float);
	private:
		bool         brainlessMove(float);
	private:
		std::vector<int> targetIndices;
		std::shared_ptr<std::vector<std::shared_ptr<Cell>>>                     cells;
		std::shared_ptr<Cell>                                  current_cell;
		float distanceX, distanceY;
		std::shared_ptr<Cell> targetCell;
		std::vector<int> getCellIndices(float posX, float posY);
		std::vector<int> testgetCellIndices(float posX, float posY);
		bool         checkInBounds();
	};
};
