#include "Food.h"
using namespace game;

Food::Food(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::string texturePath) {
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;

	this->dt = 0;
	this->frame_x = 0;
	this->frame_y = 0;
	this->u = 0;
	this->v = 0;
	this->cellWidth = 1.0f / (6);
	this->cellHeight = 1.0f / (5);
}

void Food::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());
}

int Food::consume() {
	return 50;
}

void Food::update(float dt) {

}