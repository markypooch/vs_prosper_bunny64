#pragma once
#include "Item.h"
#include "..\\bsystem\TextureManager.h"

namespace game {
	class LovePotion : public IItem
	{
	public:
		LovePotion(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~LovePotion() {};
	public:
		virtual void initialize();
		virtual void update(float);
		virtual int consume();
		virtual std::string type() { return "lovepotion"; }
	};
};
