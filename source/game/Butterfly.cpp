#include "Butterfly.h"
#include "..\\bsystem\Camera.h"
using namespace game;

Butterfly::Butterfly(int width, int height, int resolution[], float position[], std::shared_ptr<bsystem::TextureManager> textureManager, std::string texturePath, std::shared_ptr<std::vector<std::shared_ptr<Cell>>> cells) {
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;

	this->cells = cells;

	this->dt = 0;
	this->frame_x = 0;
	this->frame_y = 0;
	this->u = 0;
	this->v = 0;
	this->cellWidth = 1.0f / (6);
	this->cellHeight = 1.0f / (5);
}

Butterfly::~Butterfly() {

}

void Butterfly::update(float dt) {
	brainlessMove(dt);
}

void Butterfly::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());
}

std::vector<int> Butterfly::getCellIndices(float posX, float posY) {
	float bunnyPosition[2];
	bunnyPosition[0] = resolution[0] / 2 - posX;
	bunnyPosition[1] = (resolution[1] / 4 - (height)) - posY;

	std::vector<float> pos;
	pos.push_back(bunnyPosition[0]);
	pos.push_back(bunnyPosition[1]);

	std::vector<int> indices = bsystem::Camera::getTileCoordinates(bsystem::Camera::isoTo2D(bunnyPosition), 96);
	return indices;
}

bool Butterfly::brainlessMove(float dt) {

	if (!this->targetCell) {

		int tileLookups[8][3] = {
			{0,-1, Direction::NORTH},      {0,  1, Direction::SOUTH},      {1, 0, Direction::EAST},         {-1, 0, Direction::WEST},
			{1,-1, Direction::NORTH_EAST}, {-1,-1, Direction::NORTH_WEST}, {-1, 1, Direction::SOUTH_WEST},  {1, 1,  Direction::SOUTH_EAST}
		};

		int tile = rand() % 8;

		std::vector<int> cellIndices = getCellIndices(this->position[0], this->position[1]);
		current_cell = cells->at(cellIndices.at(0) + (cellIndices.at(1) * 10));

		int cellTargetX = abs((cellIndices.at(0) + tileLookups[tile][0]));
		int cellTargetY = abs((cellIndices.at(1) + tileLookups[tile][1]));


		if (cellTargetX > 9 || cellTargetY > 9 || cellTargetX < 0 || cellTargetY < 0) {
			return false;
		}
		std::cout << cellTargetX << " " << cellTargetY << std::endl;
		targetCell = cells->at(cellTargetX + (cellTargetY * 10));
		const float* pos = targetCell->retrievePosition();

		distanceX = pos[0] - position[0];
		distanceY = pos[1] - position[1];

		targetIndices = getCellIndices(pos[0], pos[1]);
		this->direction = (Direction)tileLookups[tile][2];

		this->u = 0;
		this->v = 0;

		this->renderSheet = true;
		this->frame_x = 0;
		this->frame_y = 0;
	}
	else {

		float movement_delta = dt / 6;

		const float* pos = targetCell->retrievePosition();

		int posX = pos[0];
		int posY = pos[1];

		if (position[0] < posX) position[0] += (distanceX)* movement_delta;
		else if (position[0] > posX) position[0] -= -((distanceX)* (movement_delta));

		if (position[1] < posY)  position[1] += (distanceY)* movement_delta;
		else if (position[1] > posY) position[1] -= -((distanceY)* movement_delta);

		if (this->renderSheet) {
			animate(dt, 30, 5, 4, 0.001f);
		}

		if (checkInBounds()) {
			targetCell = nullptr;

			this->frame_x = 0;
			this->frame_y = 0;
		}
	}
	return true;
}

bool Butterfly::checkInBounds() {
	std::vector<int> cellIndices = getCellIndices(this->position[0], this->position[1]);
	if (cellIndices[0] == targetIndices.at(0) && cellIndices[1] == targetIndices.at(1)) {
		std::cout << std::endl;
		std::cout << std::endl;
		return true;
	}
	else return false;
}
