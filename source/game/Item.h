#pragma once
#include "..\\bsystem\TextureManager.h"
#include "Sprite.h"

namespace game {
	class IItem : public Sprite
	{
	public:
		virtual ~IItem() {};
	public:
		virtual int         consume() = 0;
		virtual std::string type()    = 0;
	};
};
