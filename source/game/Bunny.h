#pragma once
#include <vector>
#include <string>
#include <random>

#include "Cell.h"
#include "Sprite.h"

namespace game {
	class Bunny : public Sprite
	{
	public:
		Bunny(int width,
			  int height,
			  int resolution[],
			  float position[],
			  std::shared_ptr<bsystem::TextureManager>               textureManager,
			  std::map<std::string, std::map<std::string, uint32_t>> texturePaths,
			  std::shared_ptr<std::vector<std::shared_ptr<Cell>>>                     cells);
		virtual ~Bunny();
	public:
		virtual void initialize();
		virtual void update(float);
	public:
		enum Direction {
			NORTH      = 0,
			SOUTH      = 1,
			EAST       = 2,
			WEST       = 3,
			NORTH_EAST = 4,
			NORTH_WEST = 5,
			SOUTH_EAST = 6,
			SOUTH_WEST = 7
		} direction;
		bool flip = false;
	private:

		bool         brainlessMove(float);
		bool         logicLessAction(float, float);
		bool         eat(float);
		bool         makeSweetLove(float);
		bool         flipping(float);
		//bool         animate(float, int, int, int, float frametime);
		std::vector<int> getCellIndices(float posX, float posY);
		std::vector<int> testgetCellIndices(float posX, float posY);
		bool         checkInBounds();
		std::shared_ptr<Cell> targetCell;


		int* isoTo2D(int pnt[]);
		int* getTileCoordinates(const int* pnt, float tileHeight);

		void setAtlas();
	private:
		std::shared_ptr<std::vector<std::shared_ptr<Cell>>>                    cells;
		std::shared_ptr<Cell>                                  current_cell;
		std::map<std::string, std::map<std::string, uint32_t>> textureHandles;
		std::vector<std::string> filenames;

		int worldDimensions[2];
		float distanceX, distanceY;

		float deathAnimationTimer;
		float bunny_hungry_timer;

		std::vector<int> targetIndices;

		float bunny_hunger;
		float bunny_happy;
		float bunny_scared;
		float bunny_overall_health;
	public:
		class ActionList {
		public:
			enum State {
				READY,
				FAILED,
				RUNNING,
				COMPLETE,
			};
			enum Behaviors {
				NONE,
				WALKING,
				LOOK_AROUND,
				SNIFF,
				SCRATCHING,
				TREADING,
				FLIPPING,
			};
			struct Action {
				bool      initialCheck = false;
				State     current_state = State::READY;
				Behaviors behavior      = Behaviors::NONE;
				std::shared_ptr<Action> neighbor = nullptr;
				Action()                             {}

				bool invokeNode() {
					if (current_state == FAILED || current_state == COMPLETE) return false;
					if (current_state == READY || current_state == RUNNING) return true;
				}
			};

			void addNode(Behaviors behavior) {
				auto nodeptr = root;
				std::shared_ptr<Action> prev;
				while (nodeptr != nullptr) {
					prev = nodeptr;
					nodeptr = nodeptr->neighbor;
				}

				nodeptr = std::make_shared<Action>();
				nodeptr->behavior = behavior;
				prev->neighbor = nodeptr;
			}

			Behaviors traverseList() {
				auto nodePtr = root;
				while (nodePtr != nullptr) {
					if (nodePtr->invokeNode()) {
						currentAction = nodePtr;
						return nodePtr->behavior;
					}
					nodePtr = nodePtr->neighbor;
				}

				//All behaviors either completed, or failed, reset list
				nodePtr = root;
				while (nodePtr != nullptr) {
					nodePtr->current_state = State::READY;
					nodePtr = nodePtr->neighbor;
				}
				currentAction = nullptr;
			}

			std::shared_ptr<Action> root;
			std::shared_ptr<Action> currentAction = nullptr;
		} actionList;
	};
};