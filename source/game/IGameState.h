#pragma once
#include <GLFW/glfw3.h>
#include <memory>
#include "..\bsystem\Input.h"

namespace game {
	class IGameState
	{
	public:
		virtual bool initialize(std::shared_ptr<bsystem::Input> input)    = 0;
		virtual void update(float, std::shared_ptr<bsystem::Input> input) = 0;
		virtual void render()											  = 0;
	};
};
