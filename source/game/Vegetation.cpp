#include "Vegetation.h"
using namespace game;

Vegetation::Vegetation(int width, int height, int resolution[], float position[], bool vegAnim, std::shared_ptr<bsystem::TextureManager> textureManager, std::string texturePath) {
	this->width = width;
	this->height = height;

	this->resolution[0] = resolution[0];
	this->resolution[1] = resolution[1];

	this->position[0] = position[0];
	this->position[1] = position[1];

	this->textureManager = textureManager;
	this->texturePath = texturePath;

	this->renderSheet = vegAnim;
	this->vegAnimCheckDt = 0;
	this->dt = 0;
	this->frame_x = 0;
	this->frame_y = 0;
	this->u = 0;
	this->v = 0;
	this->cellWidth = 1.0f / (6);
	this->cellHeight = 1.0f / (5);
	this->playingAnimation = false;
}

void Vegetation::initialize() {
	textureHandle = textureManager->loadTexture(texturePath.c_str());	
}

void Vegetation::update(float dt) {
	if (renderSheet) {
		vegAnimCheckDt += dt;
		int playAnim = 100;
		if (vegAnimCheckDt >= 10.0) {
			playAnim = rand() % 100 + 1;
			vegAnimCheckDt = 0.0;
		}
		if (playAnim < 50 || playingAnimation) {
			if (!animate(dt, 30, 5, 4, 0.09)) {
				playingAnimation = true;
			}
			else {
				this->u = 0;
				this->v = 0;
				this->frame_y = 0;
				this->frame_x = 0;
				playingAnimation = false;
			}
		}
	}

}