#include "Sprite.h"

namespace game {
	class Hud : public Sprite {
	public:
		Hud(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~Hud() {};
	public:
		virtual void initialize();
		virtual void update(float);
	public:
		int bunnyPoints = 0;
	};
}