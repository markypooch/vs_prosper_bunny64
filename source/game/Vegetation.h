#pragma once
#include "Sprite.h"
#include "..\bsystem\TextureManager.h"
namespace game {
	class Vegetation : public Sprite
	{
	public:
		Vegetation(int width,
			int   height,
			int   resolution[],
			float position[],
			bool vegAnim,
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~Vegetation() {};
	public:
		virtual void initialize();
		virtual void update(float);
	private:
		bool playingAnimation;
		float vegAnimCheckDt;
		std::string vegType;
	};
};
