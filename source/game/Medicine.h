#pragma once
#include "Item.h"
#include "..\\bsystem\TextureManager.h"

namespace game {
	class Medicine : public IItem
	{
	public:
		Medicine(int width,
			int   height,
			int   resolution[],
			float position[],
			std::shared_ptr<bsystem::TextureManager> textureManager,
			std::string texturePath);
		virtual ~Medicine() {};
	public:
		virtual void initialize();
		virtual void update(float);
		virtual int consume();
		virtual std::string type() { return "meds"; }
	};
};