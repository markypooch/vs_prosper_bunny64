#include "Sprite.h"
using namespace game;

bool Sprite::animate(float dt, int max_cell, int col_x, int col_y, float frametime) {

	bool returnStatus = false;

	this->cellWidth = 1.0f / (col_x + 1);
	this->cellHeight = 1.0f / (col_y + 1);

	this->dt += dt;
	if (this->dt >= frametime) {
		frame_x += 1;
		if ((frame_x + 1) * (frame_y + 1) > max_cell - 1) {
			frame_x = 0;
			frame_y = 0;
			returnStatus = true;
		}
		else {
			this->dt = 0.0f;
			if (frame_x > col_x) {
				frame_x = 0;
				frame_y += 1;
				if (frame_y > col_y) {
					frame_y = 0;
					returnStatus = true;
				}
			}
			u = frame_x * cellWidth;
			v = frame_y * cellHeight;
		}
	}

	return returnStatus;
}

void Sprite::render() {

	glPushMatrix();
	glLoadIdentity();

	glTranslatef(position[0], position[1], 0);
	glBindTexture(GL_TEXTURE_2D, textureHandle);
	
	glBegin(GL_QUADS);

	if (!renderSheet) {

		glTexCoord2f(1.0, 1.0);
		glVertex3f(width, height, 0.0);

		glTexCoord2f(1.0, 0.0);
		glVertex3f(width, -height, 0.0);

		glTexCoord2f(0.0, 0.0);
		glVertex3f(-width, -height, 0.0);

		glTexCoord2f(0.0, 1.0);
		glVertex3f(-width, height, 0.0);
	}
	else {
		glTexCoord2f((u + this->cellWidth), (double(v) + double(this->cellHeight)));
		glVertex3f(width, height, 0.0);

		glTexCoord2f((u + this->cellWidth), v);
		glVertex3f(width, -height, 0.0);

		glTexCoord2f(u, v);
		glVertex3f(-width, -height, 0.0);

		glTexCoord2f(u, (double(v) + double(this->cellHeight)));
		glVertex3f(-width, height, 0.0);
	}


	glEnd();
	glPopMatrix();
}